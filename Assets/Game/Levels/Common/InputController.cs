﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour {
    private Vector3 lowPassValue;
    float AccelerometerUpdateInterval = 1.0f / 60.0f;
    float LowPassKernelWidthInSeconds = 1.0f;
    private Vector3 offset;
    float LowPassFilterFactor;
    void Start() {
        lowPassValue = Input.acceleration;
        LowPassFilterFactor = AccelerometerUpdateInterval / LowPassKernelWidthInSeconds; // tweakable
    }
    private Vector3 LowPassFilterAccelerometer() {
        lowPassValue = Vector3.Lerp(lowPassValue, Input.acceleration, LowPassFilterFactor);
        return lowPassValue;
    }
    public void SetOffset(Vector3 of) {
        this.offset = of;
        this.offset.y = 0;
        this.offset.z = -this.offset.z;
    }
    public Vector3 GetAccelerometerValuesFiltered() {
        Vector3 v = LowPassFilterAccelerometer();
        v.y = 0;
        v.z = -v.z;
        //UIManager.Instance.SetMissionMessage("A:" + Input.acceleration + " Fil:" + (v - offset));
        return v - offset;
    }
    public Vector3 GetAccelerometerValues() {
        Vector3 v = Input.acceleration;
        v.y = 0;
        v.z = -v.z;
        //UIManager.Instance.SetMissionMessage("Vector:" + v + " Fil:" + (v - offset));
        return v - offset;
    }
    public bool GetLeftButton() {
        if (GameManager.Instance.GetInputType() == 0)
            return ETCInput.GetButton("Left");
        else
            return GetAccelerometerValues().x < -0.2f;
    }
    public bool GetRightButton() {
        if (GameManager.Instance.GetInputType() == 0)
            return ETCInput.GetButton("Right");
        else
            return GetAccelerometerValues().x > 0.2f;
    }
    public bool GetForwardButton() {
        if (GameManager.Instance.GetInputType() == 0)
            return ETCInput.GetButton("Forward");
        else
            //return GetAccelerometerValuesFiltered().z > -0.3f;
           return GetAccelerometerValues().z > 0.1f;
    }
    public bool GetBackwardButton() {
        if (GameManager.Instance.GetInputType() == 0)
            return ETCInput.GetButton("Backward");
        else
            //return GetAccelerometerValuesFiltered().z < 0.3f;
           return GetAccelerometerValues().z < -0.1f;
    }

}
