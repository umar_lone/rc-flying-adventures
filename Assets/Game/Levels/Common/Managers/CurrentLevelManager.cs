﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// This class is loaded in all levels. It simply gets the level manager index from GameManager and activates it
/// </summary>
public class CurrentLevelManager : MonoBehaviour {
    public enum DebugVehicle { AUTO, SIMPLE, ATTACK };
    [Header("FOR DEBUGGING ONLY")]
    public DebugVehicle currentVehicle;
    // Use this for initialization
    void Start() {
        //Load the prefab that represents the current level
        int levelNo = GameManager.Instance.GetCurrentLevelIndex();
        Transform levelTransform = GameObject.Find("_LEVELOBJECTS/Levels").transform;
        int count = levelTransform.childCount;
        GameObject[] levels = new GameObject[count];
        for (int i = 0; i < count; ++i) {
            levels[i] = levelTransform.GetChild(i).gameObject;
            levels[i].SetActive(false);
        }
        Array.Sort(levels, (go1, go2) => {
            return go1.name.CompareTo(go2.name);
        });
        GameObject levelObject = levels[levelNo].gameObject;
        levelObject.SetActive(true);
        GameObject vehicle = GameObject.Find("_VEHICLES");
        GameObject simpleHeli = vehicle.transform.FindChild("_Player - SimpleHeli").gameObject;
        GameObject attackHeli = vehicle.transform.FindChild("_Player - AttackHeli").gameObject;

        //!Only for debugging
        switch (currentVehicle) {
            case DebugVehicle.SIMPLE:
                attackHeli.SetActive(false);
                simpleHeli.SetActive(true);
                GameManager.Instance.SetCurrentVehicle(simpleHeli.transform.FindChild("Helicopter").gameObject);
                return;
            case DebugVehicle.ATTACK:
                simpleHeli.SetActive(false);
                attackHeli.SetActive(true);
                GameManager.Instance.SetCurrentVehicle(attackHeli.transform.FindChild("Helicopter-Attack").gameObject);
                return;
            case DebugVehicle.AUTO:
                //Break out and select what user chose
                break;
        }

        //switch (VehicleType.ATTACKHELICOPTER) {
        switch (GameManager.Instance.GetVehicleType()) {
            case VehicleType.SIMPLEHELICOPTER:
                attackHeli.SetActive(false);
                simpleHeli.SetActive(true);
                GameManager.Instance.SetCurrentVehicle(simpleHeli.transform.FindChild("Helicopter").gameObject);
                break;
            case VehicleType.ATTACKHELICOPTER:
                simpleHeli.SetActive(false);
                attackHeli.SetActive(true);
                GameManager.Instance.SetCurrentVehicle(attackHeli.transform.FindChild("Helicopter-Attack").gameObject);
                break;
            default:
                attackHeli.SetActive(false);
                simpleHeli.SetActive(true);
                GameManager.Instance.SetCurrentVehicle(simpleHeli.transform.FindChild("Helicopter").gameObject);
                break;
        }
    }

}
