﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Collections;

public struct GAMEPREFS {
    static readonly string PREFS_CURRENT_LEVEL = "current_level";
    static readonly int PREFS_MONEY = 0;
    public struct VIDEO {
        public static readonly string TEXTURE_REZ = "pref_texture_rez";
        public static readonly string ANTIALIASING = "pref_antialiasing";
        public static readonly string SHADOW_REZ = "pref_shadow_rez";
        public static readonly string DRAW_DISTANCE = "pref_draw_dist";
        public static readonly string PIXEL_LIGHTS = "pref_pixel_lights";
        public static readonly string SOFTVEGETATION = "pref_softvegetation";
    }
    public struct AUDIO {
        public static readonly string SOUND = "pref_sound";
        public static readonly string MUSIC = "pref_music";
    }
    public struct INPUT {
        public static readonly string INPUTTYPE = "pref_inputtype";
        public static readonly string X_OFFSET = "pref_x_offset";
        public static readonly string Y_OFFSET = "pref_y_offset";
        public static readonly string Z_OFFSET = "pref_z_offset";
    }
    public struct GAME {
        public static readonly string TIMEOFDAY = "pref_timeofday";
        public static readonly string CAMERAPOSITION = "pref_cam_position";
    }

    public struct EPISODE {
        public static readonly string SCENE_BESTTIMES_ = "pref_scene_besttimes_";
        public static readonly string SCENE_UNLOCKED_LEVELS = "pref_unlocked_levels_scene_";
    }
}

public enum VehicleType {
    ANY,
    SIMPLEHELICOPTER,
    ATTACKHELICOPTER
}
struct LevelRestartInfo {
    public int mLevelNo  ;
    public bool mUnlockedNextLevel ;
}
public class GameManager : MonoBehaviour {
    //TODO: Loading screen should be loading automatically
    public GameObject loadingScreen;
    public GameObject levelFinishedMenu;
    public GameObject missionInfo;
    public GameObject levelEndCamera;
    public GameObject levelFailed;

    private Vector3[] mVehiclePositions;

    private VehicleType mVehicleType;
    private GameObject mVehicle;
    private GameObject mCurrentLevel;
    static GameManager instance;
    private InputController mInputController;
    private int mCurrentSceneIndex = 0;
    public int mCurrentLevelIndex = 0;

    private float mTimeScale = 1.0f;
    //Video
    private int mTextureRez;
    private int mAntiAliasing;
    private int mShadowRez;
    private int mDrawDistance;
    private int mPixelLights;
    private bool mSoftVeg;

    //Audio
    private float mSound;
    private float mMusic;
    private float mMenuMusic;

    private int mQualityLevel;
    private int mCameraPosition;
    private int mTimeOfDay;

    //Input
    private int mInputType;
    private Vector3 mAccelOffset;

    private List<AudioSource> mAudioSources;
    private AudioSource mCurrentMusicSource;
    private AudioSource mMenuAudioSource;

    private const int TOTAL_SCENES = 1;
    private int[] mLevelsUnlocked = new int[TOTAL_SCENES];

    //Increment this everytime a new level is added in scene 1 (0-10  means 11 is the last level)
    public const int SCENE01_LEVELS = 13;
    public const int SCENE02_LEVELS = 0;

    private float[] mScene01_level_times = new float[SCENE01_LEVELS];
    private float[] mScene02_level_times = new float[SCENE02_LEVELS];

    private int[][] mSceneBestTimes = new int[TOTAL_SCENES][];
    private Dictionary<int, VehicleType> mLevelVehicles;

    private LevelRestartInfo mRestartInfo;

    void Awake() {
        EpicPrefs.Initialize();
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        mInputController = GetComponent<InputController>();
        mAudioSources = new List<AudioSource>();
    }


    // Use this for initialization
    void Start() {
        if (instance != null) {
            GameObject.Destroy(gameObject);
            instance.LoadSettings();
        }
        else {
            DontDestroyOnLoad(gameObject);
            instance = this;
            LoadSettings();
        }
        mVehiclePositions = new Vector3[] {
            new Vector3(318.17f, 57.948f, -771.35f) ,   //Hay farm
            new Vector3(302.72f, 85.978f, 204f),        //Lake
            new Vector3(821.8f, 82.1f, 415.3f),         //Near chicken farm
            new Vector3(709.69f, 113.618f, 981.62f),   //Coffee shop 
            new Vector3(-854f, 95.088f, 1726.56f),        //Only hay farm
            new Vector3(-998.01f, 90.448f, 411.12f),    //On road, near town
            new Vector3(-1487.8f, 90.186f, -1423.5f),    //Orchard
            new Vector3(-863.27f, 95.032f, 1722.831f)    //Corny farm
        };
    }
    public void SetLevelVehicles(Dictionary<int, VehicleType> levelVehicles) {
        mLevelVehicles = levelVehicles;
    }
    public Vector3 GetVehiclePosition() {
        return mVehiclePositions[Random.Range(0, mVehiclePositions.Length)];
    }
    public VehicleType GetVehicleType() {
        return mVehicleType;
    }
    //WARNING : Used only by helicopter selection menu
    public void SetVehicleType(VehicleType type) {
        //should be in sync with correct vehicle gameobject
        mVehicleType = type;
    }
    public GameObject GetCurrentVehicle() {
        return mVehicle;
    }
    public void SetCurrentVehicle(GameObject vehicle) {
        mVehicle = vehicle;
        switch (mVehicle.name) {
            case "Helicopter-Attack":
                mVehicleType = VehicleType.ATTACKHELICOPTER;
                break;
            case "Helicopter":
                mVehicleType = VehicleType.SIMPLEHELICOPTER;
                break;
        }
    }
    public float GetTimeScale() {
        return mTimeScale;
    }
    public void SetTimeScale(float timescale) {
        mTimeScale = timescale;
        Time.timeScale = mTimeScale;
    }

    public void SetCrossHairVisibility(bool visible) {
        if (mVehicle != null && mVehicleType == VehicleType.ATTACKHELICOPTER) {
            mVehicle.GetComponent<WeaponSystem>().SetCrossHairVisibility(visible);
        }
    }
    #region PREFERENCES
    public void LoadSettings() {
        InitSceneLevels();
        InitInputSettings();
        InitVideoSettings();
        InitAudioSettings();
        InitGameSettings();
        ApplyVideoSettings();
        SetAudioSourcesVolume(mSound);
    }
    private void InitSceneLevels() {
        for (int i = 0; i < TOTAL_SCENES; ++i) {
            mLevelsUnlocked[i] = EpicPrefs.GetInt(GAMEPREFS.EPISODE.SCENE_UNLOCKED_LEVELS + i.ToString(), -1);
            if (mLevelsUnlocked[i] == -1) {
                mLevelsUnlocked[i] = 2; //Allow player to play first two levels (first is free flight mode)
            }
            mSceneBestTimes[i] = EpicPrefs.GetArrayInt(GAMEPREFS.EPISODE.SCENE_BESTTIMES_ + i.ToString());
            if (mSceneBestTimes[i] == null) {
                //Create a float with empty values
                mSceneBestTimes[i] = new int[GetMaxLevelsInScene(i)];
                for(int level = 0; level < GetMaxLevelsInScene(i); ++level) {
                    mSceneBestTimes[i][level] = -1; 
                }
            }
        }
        //TODO Remove this
        //mLevelsUnlocked[0] = 10;
    }
    private void InitGameSettings() {
        mTimeOfDay = EpicPrefs.GetInt(GAMEPREFS.GAME.TIMEOFDAY, -1);
        if (mTimeOfDay == -1) { //EpicPrefs don't exist, so use system default
            mTimeOfDay = 0;     //Use system time
        }
        mCameraPosition = EpicPrefs.GetInt(GAMEPREFS.GAME.CAMERAPOSITION, -1);
        if (mCameraPosition == -1) {
            mCameraPosition = 1;    //Use Far as default
        }
    }

    private void InitInputSettings() {
        mInputController = GetComponent<InputController>();
        mInputType = EpicPrefs.GetInt(GAMEPREFS.INPUT.INPUTTYPE, 0);
        GameObject movementControls = GameObject.Find("ETCOnScreenControls/SimpleControls/MovementControls");
        if (movementControls != null) {
            movementControls.SetActive(mInputType == 0 ? true : false);
        }
        float x = EpicPrefs.GetFloat(GAMEPREFS.INPUT.X_OFFSET, 0);
        float y = EpicPrefs.GetFloat(GAMEPREFS.INPUT.Y_OFFSET, 0);
        float z = EpicPrefs.GetFloat(GAMEPREFS.INPUT.Z_OFFSET, 0);
        mAccelOffset = new Vector3(x, y, z);
        mInputController.SetOffset(mAccelOffset);
    }
    private void InitVideoSettings() {

        mShadowRez = EpicPrefs.GetInt(GAMEPREFS.VIDEO.SHADOW_REZ, -1);
        if (mShadowRez == -1) {     //EpicPrefs don't exist, use default
            mShadowRez = QualitySettings.GetQualityLevel();
        }

        mTextureRez = EpicPrefs.GetInt(GAMEPREFS.VIDEO.TEXTURE_REZ, -1);
        if (mTextureRez == -1) {     //EpicPrefs don't exist, use default
            mTextureRez = QualitySettings.masterTextureLimit;
        }

        mDrawDistance = EpicPrefs.GetInt(GAMEPREFS.VIDEO.DRAW_DISTANCE, -1);
        if (mDrawDistance == -1) {     //EpicPrefs don't exist, use default
            mDrawDistance = 3000;
        }
        mPixelLights = EpicPrefs.GetInt(GAMEPREFS.VIDEO.PIXEL_LIGHTS, -1);
        if (mPixelLights == -1) {     //EpicPrefs don't exist, use default
            mPixelLights = QualitySettings.pixelLightCount;
        }
        mAntiAliasing = EpicPrefs.GetInt(GAMEPREFS.VIDEO.ANTIALIASING, -1);
        if (mAntiAliasing == -1) {     //EpicPrefs don't exist, use default
            mAntiAliasing = QualitySettings.antiAliasing;
        }

        mSoftVeg = EpicPrefs.GetInt(GAMEPREFS.VIDEO.SOFTVEGETATION, 1) == 1 ? true : false;
    }
    private void InitAudioSettings() {
        mMusic = EpicPrefs.GetFloat(GAMEPREFS.AUDIO.MUSIC, -1);
        if (mMusic == -1) {     //EpicPrefs don't exist, use default
            mMusic = 1;
        }
        else {
            //Else set the global music volume
        }
        mSound = EpicPrefs.GetFloat(GAMEPREFS.AUDIO.SOUND, -1);
        if (mSound == -1) {     //EpicPrefs don't exist, use default
            mSound = 1;
        }
        else {
            //Else set the global sound volume
        }
    }
    public void ApplyVideoSettings() {
        /*
       We're setting shadows through quality levels. Since, it changes other quality
       parameters also, we set the quality settings first and then individually
       modify other quality settings based on users preference
       */
        QualitySettings.SetQualityLevel(mShadowRez, false);
        QualitySettings.masterTextureLimit = mTextureRez;
        QualitySettings.antiAliasing = mAntiAliasing;
        QualitySettings.softVegetation = mSoftVeg;
        QualitySettings.pixelLightCount = mPixelLights;
        SetPlayerCameraDrawDistance();
        //TODO Set draw distance in camera
    }

    private void SetPlayerCameraDrawDistance() {
        if (mVehicle != null) {
            GameObject parent = mVehicle.transform.parent.gameObject;
            Camera camera = parent.transform.FindChild("FollowCamera/Main Camera").gameObject.GetComponent<Camera>();
            camera.farClipPlane = mDrawDistance;
        }
    }

    #region VIDEO SETTERS & GETTERS
    public int GetDrawDistance() {
        return mDrawDistance;
    }
    public void SetDrawDistance(int distance) {
        mDrawDistance = distance;
        EpicPrefs.SetInt(GAMEPREFS.VIDEO.DRAW_DISTANCE, mDrawDistance);
        SetPlayerCameraDrawDistance();
        //TODO Set draw distance in camera
    }
    public int GetPixelLightCount() {
        return mPixelLights;
    }
    public void SetPixelLightCount(int count) {
        mPixelLights = count;
        EpicPrefs.SetInt(GAMEPREFS.VIDEO.PIXEL_LIGHTS, mPixelLights);
        QualitySettings.pixelLightCount = mPixelLights;

    }
    public void SetTextureRezSetting(int level) {
        mTextureRez = level;
        EpicPrefs.SetInt(GAMEPREFS.VIDEO.TEXTURE_REZ, mTextureRez);

        QualitySettings.masterTextureLimit = mTextureRez;
    }
    public int GetTextureRezSetting() {
        return mTextureRez;
    }

    public void SetAntiAliasingSetting(int level) {
        //TODO Maybe remove this check later???
        //if (level % 2 != 0)
        //    throw new ArgumentException("Antialiasing values should be either 0, 2, 4 or 8");
        mAntiAliasing = level;

        EpicPrefs.SetInt(GAMEPREFS.VIDEO.ANTIALIASING, mAntiAliasing);
        QualitySettings.antiAliasing = mAntiAliasing;
    }
    public int GetAntiAliasingSetting() {
        return mAntiAliasing;
    }

    public void SetShadowLevelSetting(int level) {
        mShadowRez = level;
        EpicPrefs.SetInt(GAMEPREFS.VIDEO.SHADOW_REZ, mShadowRez);
        ApplyVideoSettings();
    }
    public int GetShadowLevelSetting() {
        return mShadowRez;
    }
    public void SetSoftVegSetting(bool val) {
        mSoftVeg = val;
        EpicPrefs.SetInt(GAMEPREFS.VIDEO.SOFTVEGETATION, mSoftVeg == true ? 1 : 0);
    }
    public bool GetSoftVegSetting() {
        return mSoftVeg;
    }

    #endregion

    #region AUDIO SETTERS & GETTERS

    public void RegisterAudioSource(AudioSource source) {
        mAudioSources.Add(source);
    }
    public void UnregisterAudioSource(AudioSource source) {
        mAudioSources.Remove(source);
    }
    public void SetAudioSourcesVolume(float volume) {
        for (int i = 0; i < mAudioSources.Count; ++i) {
            if (mAudioSources[i] != null)
                mAudioSources[i].volume = volume;
        }
    }
    public void PauseAudioSources() {
        for (int i = 0; i < mAudioSources.Count; ++i) {
            if (mAudioSources[i] != null && mAudioSources[i].enabled)
                mAudioSources[i].Pause();
        }
    }
    public void ResumeAudioSources() {
        for (int i = 0; i < mAudioSources.Count; ++i) {
            if (mAudioSources[i] != null)
                mAudioSources[i].Play();
        }
    }
    public void ClearAudioSources() {
        mAudioSources.Clear();
    }
    public void SetCurrentMusicSource(AudioSource source) {
        mCurrentMusicSource = source;
    }
    public AudioSource GetCurrentMusicSource() {
        return mCurrentMusicSource;
    }
    public float GetMusicSetting() {
        return mMusic;
    }
    public void SetMusicSetting(float level) {
        mMusic = level;
        EpicPrefs.SetFloat(GAMEPREFS.AUDIO.MUSIC, mMusic);
        mCurrentMusicSource.volume = mMusic;
        //TODO Set corresponding audio type volume here
    }

    public float GetSoundSetting() {
        return mSound;
    }
    public void SetSoundSetting(float level) {
        mSound = level;
        EpicPrefs.SetFloat(GAMEPREFS.AUDIO.SOUND, mSound);
        SetAudioSourcesVolume(level);
        //TODO Set corresponding audio type volume here
    }
    #endregion

    #region MISCELLANEOUS SETTINGS
    public void SetAccelOffset(Vector3 offset) {
        mAccelOffset = offset;
        EpicPrefs.SetFloat(GAMEPREFS.INPUT.X_OFFSET, mAccelOffset.x);
        EpicPrefs.SetFloat(GAMEPREFS.INPUT.Y_OFFSET, mAccelOffset.y);
        EpicPrefs.SetFloat(GAMEPREFS.INPUT.Z_OFFSET, mAccelOffset.z);
        mInputController.SetOffset(mAccelOffset);
    }
    public Vector3 GetAccelOffset() {
        return mAccelOffset;
    }

    public void SetInputType(int type) {
        mInputType = type;
        EpicPrefs.SetInt(GAMEPREFS.INPUT.INPUTTYPE, mInputType);
    }
    public int GetInputType() {
        return mInputType;
    }
    public void SetTimeOfDaySetting(int option) {
        mTimeOfDay = option;
        EpicPrefs.SetInt(GAMEPREFS.GAME.TIMEOFDAY, mTimeOfDay);

        GameObject go = GameObject.Find("DayNightController");
        if (go != null) {
            DayNightController dnc = go.GetComponent<DayNightController>();
            dnc.SetTimeOfDayOption();
        }//else the user has opened the menu from the Main Menu, so don't do anything
    }
    public int GetTimeOfDaySetting() {
        return mTimeOfDay;
    }
    public void SetCameraPosition(int pos) {
        mCameraPosition = pos;
        EpicPrefs.SetInt(GAMEPREFS.GAME.CAMERAPOSITION, mCameraPosition);
        if (mVehicle != null) {
            GameObject parent = mVehicle.transform.parent.gameObject;
            MeshRenderer[] renderer = mVehicle.GetComponentsInChildren<MeshRenderer>();
            bool rendererEnabled = false;
            Transform cameraTransform = parent.transform.FindChild("FollowCamera/Main Camera").transform;
            switch (mCameraPosition) {
                case 0:
                    rendererEnabled = false;
                    cameraTransform.localPosition = new Vector3(0, .54f, 0);
                    break;
                case 1:
                    rendererEnabled = true;
                    switch (mVehicleType) {
                        case VehicleType.SIMPLEHELICOPTER:
                            cameraTransform.localPosition = new Vector3(0, 1, -2.24f);
                            break;
                        case VehicleType.ATTACKHELICOPTER:
                            cameraTransform.localPosition = new Vector3(0, 1, -2.76f);
                            break;
                    }
                    break;
                case 2:
                    rendererEnabled = true;
                    cameraTransform.localPosition = new Vector3(0, 1.4f, -4);
                    break;
            }
            for (int i = 0; i < renderer.Length; ++i) {
                renderer[i].enabled = rendererEnabled;
            }
        }
    }
    public int GetCameraPosition() {
        return mCameraPosition;
    }
    #endregion
    #endregion

    public int[] GetSceneBestTimes(int scene) {
        return mSceneBestTimes[scene];
    }
    #region SCENE & LEVEL MANAGEMENT METHODS
    public int GetMaxLevelsInScene(int episode) {
        switch (episode) {
            case 0:
                return SCENE01_LEVELS;
            case 1:
                return SCENE02_LEVELS;
            default:
                throw new System.Exception("Wrong episode index");
        }
    }
    public int GetLevelsUnlockedForScene(int scene) {
        if (scene > TOTAL_SCENES)
            throw new System.Exception("Episode does not exist");
        return mLevelsUnlocked[scene];
    }
    public int GetLevelsUnlockedForCurrentScene() {
        return GetLevelsUnlockedForScene(mCurrentSceneIndex);
    }
    

    private void SetCurrentLevelBestTime(int seconds) {
        mSceneBestTimes[mCurrentSceneIndex][mCurrentLevelIndex] = seconds;
        EpicPrefs.SetArray(GAMEPREFS.EPISODE.SCENE_BESTTIMES_ + mCurrentSceneIndex.ToString(), mSceneBestTimes[mCurrentSceneIndex]);
    }

    public int GetCurrentLevelIndex() {
        return mCurrentLevelIndex;
    }
    public void SetCurrentLevelIndex(int level) {
        mCurrentLevelIndex = level;
    }
    public int GetCurrentSceneIndex() {
        return mCurrentSceneIndex;
    }
    public void SetCurrentSceneIndex(int sceneIndex) {
        mCurrentSceneIndex = sceneIndex;
    }
    public void LoadSelectedScene() {
        SetTimeScale(1);
        Instantiate(loadingScreen);
    }
    //Invoked when next mission button is clicked after successful mission
    public void LoadNextScene() {
        mCurrentLevelIndex += 1;
        if (mLevelVehicles.ContainsKey(mCurrentLevelIndex)) {
            VehicleType vehicle = mLevelVehicles[mCurrentLevelIndex];
            //Choose helicopter randomly when ANY vehicle is allowed
            if (vehicle == VehicleType.ANY) {
                switch (Random.Range(0, 2)) {
                    case 0:
                        vehicle = VehicleType.SIMPLEHELICOPTER;
                        break;
                    case 1:
                        vehicle = VehicleType.ATTACKHELICOPTER;
                        break;
                }
            }
            SetVehicleType(vehicle);
        }
        SetTimeScale(1f);
        Instantiate(loadingScreen);
    }

    public void MissionFailed(string message) {
        GameObject go = Instantiate(levelFailed);
        go.GetComponent<PauseController>().SetFailureMessage(message);
    }
    public void MissionFinished(string message) {
        UIManager.Instance.SetArrowVisibility(false);
        SetCrossHairVisibility(false);  //For attack helicopter
        StartCoroutine(MissionEnd(message));
    }

    private IEnumerator MissionEnd(string message) {
        Instantiate(levelEndCamera);
        yield return new WaitForSeconds(3);
        SetTimeScale(0f);
        int completionTime = mCurrentLevel.GetComponent<LevelManagerBase>().GetCompletionTime();
        int oldCompletionTime = GetSceneBestTimes(mCurrentSceneIndex)[mCurrentLevelIndex];
        if (oldCompletionTime == -1) {   //Not played yet, so old completion time is -1
            SetCurrentLevelBestTime(completionTime);
            message = "Completion Time :" + TimeHelper.GetTimeFromSeconds(completionTime);
        }
        else {
            if (completionTime < oldCompletionTime) {
                message = "New best time : " + TimeHelper.GetTimeFromSeconds(completionTime) +
                    "\nOld time : " + TimeHelper.GetTimeFromSeconds(oldCompletionTime);
                SetCurrentLevelBestTime(completionTime);
            }
            else {
                message = "Completion Time :" + TimeHelper.GetTimeFromSeconds(completionTime);
            }
        }
        GameObject go = Instantiate(levelFinishedMenu);
        go.GetComponent<UILevelFinished>().SetMessage(message);
        yield return new WaitForSeconds(0);
    }

    public void UnlockNextLevelForScene(int scene) {
        if(mRestartInfo.mLevelNo == mCurrentLevelIndex && mRestartInfo.mUnlockedNextLevel == true){
            return;
        }
        if((mCurrentLevelIndex + 1) < mLevelsUnlocked[scene]) {
            //Scene is already unlocked, so don't do anything
            return;
        }
        mLevelsUnlocked[scene]++;
        mRestartInfo.mLevelNo = mCurrentLevelIndex;
        mRestartInfo.mUnlockedNextLevel = true;
        EpicPrefs.SetInt(GAMEPREFS.EPISODE.SCENE_UNLOCKED_LEVELS + scene.ToString(), mLevelsUnlocked[scene]);
    }
    public void RestartScene() {
        mRestartInfo.mLevelNo = mCurrentLevelIndex;
        mRestartInfo.mUnlockedNextLevel = false;
        ClearAudioSources();
        SetTimeScale(1f);
        Instantiate(loadingScreen);
    }

    public void SetCurrentLevel(GameObject o) {
        mCurrentLevel = o;
    }

    public GameObject GetCurrentLevel() {
        return mCurrentLevel;
    }
    #endregion

    public void ShowMainMenu() {
        SetTimeScale(1f);
        SceneManager.LoadScene(0);
    }

    public InputController Input {
        get {
            return mInputController;
        }
    }
    public void ShowMissionInfo() {
        GameObject go = Instantiate(missionInfo);
        string missionText = mCurrentLevel.GetComponent<LevelManagerBase>().GetMissionMessage();

        go.GetComponent<UIMissionInfo>().SetTitle("Objective");
        go.GetComponent<UIMissionInfo>().SetMissionText(missionText);
    }
    public static GameManager Instance {
        get { return instance; }
    }

}
