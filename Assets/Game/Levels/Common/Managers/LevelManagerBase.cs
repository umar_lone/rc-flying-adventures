﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class TimeHelper {
    public static string GetTimeFromSeconds(int seconds) {
        string timeString;
        TimeSpan time = TimeSpan.FromSeconds(seconds);
        if (time.Seconds < 10) {
            //This is only to show 0 before second when it is less than 10
            string t = string.Format("0{0}", time.Seconds);
            timeString = String.Format("{0}:{1}", time.Minutes, t);
        }
        else {
            timeString = String.Format("{0}:{1}", time.Minutes, time.Seconds);
        }
        return timeString;
    }
}
public abstract class LevelManagerBase : MonoBehaviour {
    [Header("Basic Level Info")]
    public string levelDescription;
    protected Text mTextTime;
    protected int mActualCurrentTime = 0;
    protected int mCurrentTime = 0;
    protected bool mMissionFinished = false;
	// Use this for initialization
	void Start () {
        GameManager.Instance.SetCurrentLevel(gameObject);
        GameObject go = GameObject.Find("ETCOnScreenControls");
        mTextTime = go.transform.FindChild("OnScreenUI/Text-Time").GetComponent<Text>();
        //Only used for testing
        //GameManager.Instance.MissionFinished("");
        OnStart();
	}
    public virtual void StartLevelTimer() {
        StartCoroutine(CountdownTimer());
    }

    private IEnumerator CountdownTimer() {
        //Wait 4 seconds before the countdown starts
        yield return new WaitForSeconds(4);
        mCurrentTime = 0;
        while (true) {
            TimeSpan time = TimeSpan.FromSeconds(mCurrentTime++);
            if (time.Seconds < 10) {
                //This is only to show 0 before second when it is less than 10
                string t = string.Format("0{0}", time.Seconds);
                mTextTime.text = String.Format("{0}:{1}", time.Minutes, t);
            } else {
                mTextTime.text = String.Format("{0}:{1}", time.Minutes, time.Seconds);
            }
            yield return new WaitForSeconds(1f);
            if (mMissionFinished)
                yield return null;
        }
    }
    //Can be overridden by levels that have countdown timers
    public  virtual int GetCompletionTime() {
        return mActualCurrentTime;
    }
    public abstract void OnStart();
    public abstract string GetMissionMessage();
}
