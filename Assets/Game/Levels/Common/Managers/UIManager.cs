﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class UIManager : MonoBehaviour {
    private Text messageText;
    private static UIManager instance ;
    private GameObject mObjArrow;
    private GameObject mCurrentObjectiveItem;

    void Awake() {
        instance = this;
    }
    void Start() {
        GameObject vehicle = GameObject.Find("_VEHICLES");
        switch (GameManager.Instance.GetVehicleType()) {
            case VehicleType.SIMPLEHELICOPTER:
                mObjArrow = vehicle.transform.FindChild("_Player - SimpleHeli/FollowCamera/Main Camera/Arrow").gameObject;
                break;
            case VehicleType.ATTACKHELICOPTER:
                mObjArrow = vehicle.transform.FindChild("_Player - AttackHeli/FollowCamera/Main Camera/Arrow").gameObject;
                break;
            default:
                break;
        }
        //GameObject message = GameObject.Find("ETCOnScreenControls/MissionText/Panel/Text");
        //messageText = message.GetComponent<Text>();
    }
    void Update() {
        if (mObjArrow != null && mCurrentObjectiveItem != null) {
            mObjArrow.transform.LookAt(mCurrentObjectiveItem.transform);
        }
    }
    public void SetObjectiveItem(GameObject go) {
        mCurrentObjectiveItem = go;
    }
    public void SetArrowVisibility(bool visible) {
        mObjArrow.SetActive(visible);
    }
    public void SetMissionMessage(string message) {
        //messageText.text = message;
    }
    public void SetWarningMessage(string message, float seconds = 0f) {
        //missionWarningPanel.gameObject.SetActive(true);
        //warningText.text = message;
        //if (seconds > 0)
        //    StartCoroutine(RemoveMessageAfter(warningText, seconds));
    }
    //public void SetMissionMessage(string message, float seconds = 0f) {
    //    //missionText.gameObject.SetActive(true);
    //    //missionText.text = message;
    //    //if(seconds > 0)
    //    //    StartCoroutine(RemoveMessageAfter(missionText, seconds));
    //}

    private IEnumerator RemoveMessageAfter(Text text, float seconds) {
        for (int i = 0; i < seconds; ++i) {
            yield return new WaitForSeconds(1);
        }
        text.text = "";
       // missionWarningPanel.gameObject.SetActive(false);
    }

    public static UIManager Instance{
        get {
            return instance;
        }
    }
   
    public void OnLoadCheckPoint() {
        //Time.timeScale = 1;
        print("not implemented yet");

    }
    public void OnRestart() {
        GameManager.Instance.RestartScene();
    }
    public void OnMenu() {
        GameManager.Instance.ShowMainMenu();
    }

}
