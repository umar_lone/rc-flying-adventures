﻿using UnityEngine;
using System.Collections;

public interface OnCollisionListener {
    void OnEnter(GameObject thisObject, Collision other);
    void OnExit(GameObject thisObject, Collision other);
    void OnStay(GameObject thisObject, Collision other);
}
