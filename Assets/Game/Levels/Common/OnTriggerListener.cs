﻿using UnityEngine;
using System.Collections;

public interface OnTriggerListener  {
    void OnEnter(GameObject thisObject, Collider other);
    void OnExit(GameObject thisObject,Collider other);
    void OnStay(GameObject thisObject, Collider other);
}
