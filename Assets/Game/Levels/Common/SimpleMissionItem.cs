﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// Represents a simple mission item that just calls into the corresponding manager during trigger events
/// </summary>
public class SimpleMissionItem : MonoBehaviour {
    public string objectiveMessage;
    private OnTriggerListener mTriggerListener;
    private bool mCollected;
    public bool Collected() {
        return mCollected;
    }
    public string GetObjectiveMessage() {
        return objectiveMessage;
    }
    public void SetCollected(bool collected) {
        mCollected = collected;
    }
    public void SetOnTriggerListener(OnTriggerListener listener) {
        mTriggerListener = listener;
    }
    void OnTriggerEnter(Collider other) {
        if(mTriggerListener != null)
            mTriggerListener.OnEnter(gameObject, other);
        else {
            print("mTriggerListener IS NULL FOR [" + gameObject.name + "]");
        }
    }
    void OnTriggerStay(Collider other) {
        if(mTriggerListener != null)
                mTriggerListener.OnStay(gameObject, other);
        else {
            print("mTriggerListener IS NULL FOR [" + gameObject.name + "]");
        }
    }
    void OnTriggerExit(Collider other) {
        if(mTriggerListener != null)
                mTriggerListener.OnExit(gameObject, other);
        else {
            print("mTriggerListener IS NULL FOR [" + gameObject.name + "]");
        }
    }
}
