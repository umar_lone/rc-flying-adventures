﻿using UnityEngine;
using System.Collections;

public class Level00Manager : LevelManagerBase, OnTriggerListener {
    public string missionMessage;
    // Use this for initialization
    public override void OnStart() {
        UIManager.Instance.SetArrowVisibility(false);
        GameManager.Instance.ShowMissionInfo();
    }
    public override string GetMissionMessage() {
        return missionMessage;
    }
    public void OnEnter(GameObject thisObject, Collider other) {

    }

    public void OnExit(GameObject thisObject, Collider other) {
    }

    public void OnStay(GameObject thisObject, Collider other) {
    }
}
