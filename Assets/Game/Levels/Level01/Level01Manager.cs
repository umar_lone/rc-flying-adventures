﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

public class Level01Manager : LevelManagerBase, OnTriggerListener {
	private GameObject[] mItems;
	private int mItemsCollected = 0;
	private int TOTALITEMS;

    // Use this for initialization
    public override void OnStart() {
		Transform missionItemsTransform = transform.FindChild("MissionItems");
		TOTALITEMS = missionItemsTransform.childCount;
		mItems = new GameObject[TOTALITEMS];
		for (int i = 0; i < TOTALITEMS; ++i) {
			mItems [i] = missionItemsTransform.GetChild(i).gameObject;
			SimpleMissionItem item = mItems [i].GetComponent<SimpleMissionItem>();
			item.SetOnTriggerListener(this);
			mItems [i].SetActive(false);
		}
		Array.Sort(mItems, (o1, o2) => {
			return o1.name.CompareTo(o2.name);
		});
		mItems [0].SetActive(true);
		UpdateUI();
        //Show mission info only in the beginning
        GameManager.Instance.ShowMissionInfo();
    }

    public override string GetMissionMessage() {
        return mItems[mItemsCollected].GetComponent<SimpleMissionItem>().GetObjectiveMessage();
    }
    private void UpdateUI() {
		UIManager.Instance.SetObjectiveItem(mItems [mItemsCollected]);
        //We don't want to show mission info after picking every ring
        //so showmissioninfo does not occur here
    }


    public void OnEnter(GameObject thisObject, Collider other) {
		if (other.tag != "Player") {
			return;
		}
		int selected = Array.IndexOf(mItems, thisObject);
		if (selected != -1) {
			thisObject.GetComponent<SimpleMissionItem>().SetCollected(true);
			thisObject.SetActive(false);
            if(mItemsCollected == 0) {
                //Start the level timer only after the first ring is picked up
                StartLevelTimer();
            }
            if (++mItemsCollected == TOTALITEMS) {
                mActualCurrentTime = mCurrentTime;
                GameManager.Instance.MissionFinished("");
				return;
			}
			mItems [mItemsCollected].SetActive(true);
			UpdateUI();
		}
	}

	public void OnExit(GameObject thisObject, Collider other) {
	}

	public void OnStay(GameObject thisObject, Collider other) {
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.red;
        Transform missionItemsTransform = transform.FindChild("MissionItems");
        for (int i = 0; i < (missionItemsTransform.childCount - 1); ++i) {
			Gizmos.DrawLine(missionItemsTransform.GetChild(i).transform.position, missionItemsTransform.GetChild(i + 1).transform.position);
		}
	}
}
