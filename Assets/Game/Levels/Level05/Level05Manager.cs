﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Collections;

public class Level05Manager : LevelManagerBase, OnCollisionListener {
    private GameObject[] mItems;
    private int mItemsCollected = 0;
    private int TOTALITEMS;
    public string missionMessage;
    private AudioSource[] mAudioSources;
    private bool mAllBottlesFallen = false;
    private Text mTextMessage;


    // Use this for initialization
    public override void OnStart() {
        mTextMessage = transform.FindChild("Canvas/Text-Message").GetComponent<Text>();
        mTextMessage.text = "";
        Transform missionItemsTransform = transform.FindChild("MissionItems");
        TOTALITEMS = missionItemsTransform.childCount;
        mItems = new GameObject[TOTALITEMS];
        for (int i = 0; i < TOTALITEMS; ++i) {
            mItems[i] = missionItemsTransform.GetChild(i).gameObject;
            SimpleCollisionMissionItem item = mItems[i].GetComponent<SimpleCollisionMissionItem>();
            item.SetOnCollisionListener(this);
            mItems[i].SetActive(true);
        }
        Array.Sort(mItems, (o1, o2) => {
            return o1.name.CompareTo(o2.name);
        });
        //mItems[0].SetActive(true);
        InitAudio();
        UIManager.Instance.SetArrowVisibility(false);
        //UpdateUI();
        GameManager.Instance.ShowMissionInfo();
        //StartLevelTimer();
        StartCoroutine(BottleFallCheck());
    }
    void InitAudio() {
        mAudioSources = new AudioSource[TOTALITEMS];
        for (int i = 0; i < TOTALITEMS; ++i) {
            mAudioSources[i] = mItems[i].GetComponent<AudioSource>();
            GameManager.Instance.RegisterAudioSource(mAudioSources[i]);
            mAudioSources[i].volume = GameManager.Instance.GetSoundSetting();
            mAudioSources[i].playOnAwake = false;
            mAudioSources[i].loop = false;
            mAudioSources[i].Stop();
        }

    }
    private IEnumerator BottleFallCheck() {
        yield return new WaitForSeconds(1f);
        mTextMessage.text = "Remaining bottles:8";
        int toppled = 0;
        while (!mAllBottlesFallen) {
            yield return new WaitForSeconds(1f);
            mAllBottlesFallen = true;
            for (int i = 0; i < TOTALITEMS; ++i) {
                if (mItems[i].GetComponent<SimpleCollisionMissionItem>().IsCollected())
                    continue;

                var dot = Vector3.Dot(mItems[i].transform.up, Vector3.up);
                int x = (int) mItems[i].transform.eulerAngles.x;
                int y = (int) mItems[i].transform.eulerAngles.y;
                int z = (int) mItems[i].transform.eulerAngles.z;
                if((x >=5 && x <=355) || (x <=-5 && x >= -355) ||
                    (y >= 5 && y <= 355) || (y <= -5 && y >= -355) ||
                    (z >= 5 && z <= 355) || (z <= -5 && z >= -355)) { 
                //if (x != 0 || y != 0 || z != 0) {
                    if (toppled == 0) {
                        StartLevelTimer();
                    }
                    mItems[i].GetComponent<SimpleCollisionMissionItem>().SetCollected(true);
                    ++toppled;
                    mTextMessage.text = String.Format("Remaining bottles:{0}", (TOTALITEMS - toppled));
                }
                else {
                    mAllBottlesFallen = false;
                }
            }
        }
        mActualCurrentTime = mCurrentTime;
        MissionFinished();
        yield return null;
    }
    public override string GetMissionMessage() {
        return missionMessage;
    }
    private void UpdateUI() {
        UIManager.Instance.SetObjectiveItem(mItems[mItemsCollected]);
    }

    private void MissionFinished() {
        mMissionFinished = true;
        GameManager.Instance.MissionFinished("");
    }
    public void OnEnter(GameObject thisObject, Collision other) {
        thisObject.GetComponent<AudioSource>().Play();
        //switch (other.gameObject.tag) {
        //    case "MISSILE":
        //    case "Player":
        //        if (thisObject.GetComponent<SimpleCollisionMissionItem>().IsCollected())
        //            return;
        //        int selected = Array.IndexOf(mItems, thisObject);
        //        if (selected != -1) {
        //            thisObject.GetComponent<SimpleCollisionMissionItem>().SetCollected(true);
        //            if (++mItemsCollected == TOTALITEMS) {
        //                MissionFinished();
        //                return;
        //            }
        //            UpdateUI();
        //        }
        //        break;
        //}
    }

    public void OnExit(GameObject thisObject, Collision other) {
    }

    public void OnStay(GameObject thisObject, Collision other) {
    }

    void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Transform missionItemsTransform = transform.FindChild("MissionItems");
        //TODO Sort the items before displaying
        for (int i = 0; i < (missionItemsTransform.childCount - 1); ++i) {
            Gizmos.DrawLine(missionItemsTransform.GetChild(i).transform.position, missionItemsTransform.GetChild(i + 1).transform.position);
        }
    }
}
