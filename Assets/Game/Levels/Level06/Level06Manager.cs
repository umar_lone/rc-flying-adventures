﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;
public class Level06Manager : LevelManagerBase, OnCollisionListener {
    private GameObject[] mItems;
    public CarController busController;
    private int mItemsCollected = 0;
    private int TOTALITEMS;
    public string missionMessage;
    private AudioSource[] mAudioSources;
    private bool mAllBottlesFallen = false;
    private GameObject customObjectiveItem;
    private bool mLevelTimerStarted = false;
    
    // Use this for initialization
    public override void OnStart() {
        customObjectiveItem = transform.FindChild("POIBus/BigTruck").gameObject;
        Transform missionItemsTransform = transform.FindChild("MissionItems");
        TOTALITEMS = missionItemsTransform.childCount;
        mItems = new GameObject[TOTALITEMS];
        for (int i = 0; i < TOTALITEMS; ++i) {
            mItems[i] = missionItemsTransform.GetChild(i).gameObject;
            SimpleCollisionMissionItem item = mItems[i].GetComponent<SimpleCollisionMissionItem>();
            item.SetOnCollisionListener(this);
            mItems[i].SetActive(false);
        }
        Array.Sort(mItems, (o1, o2) => {
            return o1.name.CompareTo(o2.name);
        });
        InitAudio();
        UpdateUI();
        GameManager.Instance.ShowMissionInfo();
        StartCoroutine(BottleFallCheck());
    }

    private IEnumerator BottleFallCheck() {
        //Wait for 2 seconds for the bus to settle down
        yield return new WaitForSeconds(2f);
        for (int i = 0; i < TOTALITEMS; ++i) {
            mItems[i].SetActive(true);

        }
        /*Once the bus settles and the bottles have been put
        into the basket, start the bus
        */
        yield return new WaitForSeconds(4f);
        busController.MaxSpeed = 20;
        while (!mAllBottlesFallen) {
            yield return new WaitForSeconds(.5f);
            mAllBottlesFallen = true;
            for(int i = 0; i < TOTALITEMS; ++i) {
                if (mItems[i].GetComponent<SimpleCollisionMissionItem>().IsCollected())
                    continue ;
                Rigidbody bottle = mItems[i].GetComponent<Rigidbody>();
                if(bottle.velocity.magnitude <= .5f) {
                    mItems[i].GetComponent<SimpleCollisionMissionItem>().SetCollected(true);
                }
                else {
                    mAllBottlesFallen = false;
                }
            }
        }
        mActualCurrentTime = mCurrentTime;
        MissionFinished();
        yield return null;
    }

    void InitAudio() {
        mAudioSources = new AudioSource[TOTALITEMS];
        for(int i = 0; i < TOTALITEMS; ++i) {
            mAudioSources[i] = mItems[i].GetComponent<AudioSource>();
            GameManager.Instance.RegisterAudioSource(mAudioSources[i]);
            mAudioSources[i].volume = GameManager.Instance.GetSoundSetting();
            mAudioSources[i].playOnAwake = false;
            mAudioSources[i].loop = false;
            mAudioSources[i].Stop();
        }
        
    }
    public override string GetMissionMessage() {
        return missionMessage;
    }
    private void UpdateUI() {
        //Set the objective item to basket here
        UIManager.Instance.SetObjectiveItem(customObjectiveItem);
    }

    private void MissionFinished() {
        GameManager.Instance.MissionFinished("");

    }
    public void OnEnter(GameObject thisObject, Collision other) {
        thisObject.GetComponent<AudioSource>().Play();
        switch (other.gameObject.tag) {
            case "MISSILE":
            case "Player":
                if (!mLevelTimerStarted) {
                    StartLevelTimer();
                    mLevelTimerStarted = true;
                }
                break;
        }
    }

    public void OnExit(GameObject thisObject, Collision other) {
    }

    public void OnStay(GameObject thisObject, Collision other) {
    }


    void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Transform missionItemsTransform = transform.FindChild("MissionItems");
        for (int i = 0; i < (missionItemsTransform.childCount - 1); ++i) {
            Gizmos.DrawLine(missionItemsTransform.GetChild(i).transform.position, missionItemsTransform.GetChild(i + 1).transform.position);
        }
    }
}
