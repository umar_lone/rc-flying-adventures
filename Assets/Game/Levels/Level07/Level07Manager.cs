﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class Level07Manager : LevelManagerBase, OnTriggerListener {
    private GameObject[] mItems;
    private int mItemsCollected = 0;
    private int TOTALITEMS;
    public string missionMessage;
    private AudioSource[] mAudioSources;

    public Transform ballonEffect;
    // Use this for initialization
    public override void OnStart() {
        Transform missionItemsTransform = transform.FindChild("MissionItems");
        TOTALITEMS = missionItemsTransform.childCount;
        mItems = new GameObject[TOTALITEMS];
        for (int i = 0; i < TOTALITEMS; ++i) {
            mItems[i] = missionItemsTransform.GetChild(i).gameObject;
            SimpleMissionItem item = mItems[i].GetComponent<SimpleMissionItem>();
            item.SetOnTriggerListener(this);
            mItems[i].SetActive(false);
        }
        Array.Sort(mItems, (o1, o2) => {
            return o1.name.CompareTo(o2.name);
        });
        mItems[0].SetActive(true);
        InitAudio();
        UpdateUI();
        GameManager.Instance.ShowMissionInfo();
    }
    void InitAudio() {
        mAudioSources = new AudioSource[TOTALITEMS];
        for (int i = 0; i < TOTALITEMS; ++i) {
            mAudioSources[i] = mItems[i].GetComponent<AudioSource>();
            GameManager.Instance.RegisterAudioSource(mAudioSources[i]);
            mAudioSources[i].volume = GameManager.Instance.GetSoundSetting();
            mAudioSources[i].playOnAwake = false;
            mAudioSources[i].loop = false;
            mAudioSources[i].Stop();
        }
    }
    public override string GetMissionMessage() {
        return missionMessage;
    }
    private void UpdateUI() {
        UIManager.Instance.SetObjectiveItem(mItems[mItemsCollected]);
    }

    private void MissionFinished() {
        mMissionFinished = true;
        GameManager.Instance.MissionFinished("");
    }
   
    void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Transform missionItemsTransform = transform.FindChild("MissionItems");
        for (int i = 0; i < (missionItemsTransform.childCount - 1); ++i) {
            Gizmos.DrawLine(missionItemsTransform.GetChild(i).transform.position, missionItemsTransform.GetChild(i + 1).transform.position);
        }
    }

    public void OnEnter(GameObject thisObject, Collider other)
    {
        switch(other.gameObject.tag)
        {
            case "MISSILE":
            case "Player":
                thisObject.GetComponent<AudioSource>().Play();
                int selected = Array.IndexOf(mItems, thisObject);
                if (selected != -1)
                {
                    thisObject.GetComponent<SimpleMissionItem>().SetCollected(true);
                    Instantiate(ballonEffect, thisObject.transform.position, thisObject.transform.rotation);
                    thisObject.transform.FindChild("Group").gameObject.SetActive(false);
                    thisObject.GetComponent<Collider>().enabled = false;
                    //Cannot use setactive here, as the audiosource also gets disabled.
                    //thisObject.SetActive(false)
                    if (mItemsCollected == 0) {
                        //Start the level timer only after the first ring is picked up
                        StartLevelTimer();
                    }
                    if (++mItemsCollected == TOTALITEMS)
                    {
                        mActualCurrentTime = mCurrentTime;
                        MissionFinished();
                        return;
                    }
                    mItems[mItemsCollected].SetActive(true);
                    UpdateUI();
                }
                break;
        }
    }

    public void OnExit(GameObject thisObject, Collider other)
    {
       // throw new NotImplementedException();
    }

    public void OnStay(GameObject thisObject, Collider other)
    {
        //throw new NotImplementedException();
    }
}
