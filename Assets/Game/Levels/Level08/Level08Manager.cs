﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections;

public class Level08Manager : LevelManagerBase, OnTriggerListener
{
    public int missionTimeSeconds = 60;
    private GameObject[] mItems;
    private int mItemsCollected = 0;
    private int TOTALITEMS;
  
    // Use this for initialization
    public override void OnStart()
    {
        Transform missionItemsTransform = transform.FindChild("MissionItems");
        TOTALITEMS = missionItemsTransform.childCount;
        mItems = new GameObject[TOTALITEMS];
        for (int i = 0; i < TOTALITEMS; ++i)
        {
            mItems[i] = missionItemsTransform.GetChild(i).gameObject;
            SimpleMissionItem item = mItems[i].GetComponent<SimpleMissionItem>();
            item.SetOnTriggerListener(this);
            mItems[i].SetActive(false);
        }
        Array.Sort(mItems, (o1, o2) => {
            return o1.name.CompareTo(o2.name);
        });
        mItems[0].SetActive(true);
        UpdateUI();
        //StartLevelTimer();
    }
    public override int GetCompletionTime() {
        return missionTimeSeconds - mActualCurrentTime;
    }
    public override string GetMissionMessage()
    {
        return mItems[mItemsCollected].GetComponent<SimpleMissionItem>().GetObjectiveMessage();
    }
    private void UpdateUI()
    {
        UIManager.Instance.SetObjectiveItem(mItems[mItemsCollected]);
        GameManager.Instance.ShowMissionInfo();
    }

    private void MissionFinished()
    {
        mMissionFinished = true;
        GameManager.Instance.MissionFinished("");

    }
    public void OnEnter(GameObject thisObject, Collider other)
    {
        if (other.tag != "Player")
        {
            return;
        }
        int selected = Array.IndexOf(mItems, thisObject);
        if (selected != -1)
        {
            thisObject.GetComponent<SimpleMissionItem>().SetCollected(true);
            thisObject.SetActive(false);
            if(mItemsCollected == 0) {
                StartCoroutine(CountdownTimer());
            }
            if (++mItemsCollected == TOTALITEMS)
            {
                mActualCurrentTime = mCurrentTime;
                mMissionFinished = true;
                MissionFinished();
                return;
            }
            mItems[mItemsCollected].SetActive(true);
            UpdateUI();
        }
    }

    private IEnumerator CountdownTimer() {
        mCurrentTime = missionTimeSeconds;
        yield return new WaitForSeconds(4);
        while(mCurrentTime > 0) {
            TimeSpan time = TimeSpan.FromSeconds(mCurrentTime--);
            if(time.Seconds < 10) {
                //This is only to show 0 before second when it is less than 10
                string t = string.Format("0{0}", time.Seconds);
                mTextTime.text = String.Format("{0}:{1}", time.Minutes, t);
            }
            else {
                mTextTime.text = String.Format("{0}:{1}", time.Minutes, time.Seconds);
            }
            yield return new WaitForSeconds(1f);
            if (mMissionFinished)
                yield return null;
        }
        GameManager.Instance.MissionFailed("You ran out of time");
        yield return null;
    }

    public void OnExit(GameObject thisObject, Collider other)
    {
    }

    public void OnStay(GameObject thisObject, Collider other)
    {
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Transform missionItemsTransform = transform.FindChild("MissionItems");
        for (int i = 0; i < (missionItemsTransform.childCount - 1); ++i)
        {
            Gizmos.DrawLine(missionItemsTransform.GetChild(i).transform.position, missionItemsTransform.GetChild(i + 1).transform.position);
        }
    }
}
