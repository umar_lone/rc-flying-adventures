﻿using UnityEngine;
using System.Collections;
using System;

public class Level11Manager : LevelManagerBase, OnTriggerListener {
    private GameObject[] mItems;
    private int mItemsCollected = 0;
    private int TOTALITEMS;
    private GameObject mCanvas;

    // Use this for initialization
    public override void OnStart() {
        mCanvas = transform.FindChild("Canvas").gameObject;
        mCanvas.SetActive(false);
        Transform missionItemsTransform = transform.FindChild("MissionItems");
        TOTALITEMS = missionItemsTransform.childCount;
        mItems = new GameObject[TOTALITEMS];
        for (int i = 0; i < TOTALITEMS; ++i) {
            mItems[i] = missionItemsTransform.GetChild(i).gameObject;
            SimpleMissionItem item = mItems [i].GetComponent<SimpleMissionItem>();
            item.SetOnTriggerListener(this);
            mItems[i].SetActive(false);
        }
        Array.Sort(mItems, (o1, o2) => {
            return o1.name.CompareTo(o2.name);
        });
        mItems[0].SetActive(true);

        UpdateUI();
    }
    public override string GetMissionMessage() {
        return mItems[mItemsCollected].GetComponent<SimpleMissionItem>().GetObjectiveMessage();
    }
    private void UpdateUI() {
        UIManager.Instance.SetObjectiveItem(mItems[mItemsCollected]);
        GameManager.Instance.ShowMissionInfo();
    }

    private void MissionFinished() {
        mMissionFinished = true;
        GameManager.Instance.MissionFinished("");

    }
    public void OnEnter(GameObject thisObject, Collider other) {
        if (other.tag != "Player") {
            return;
        }
        int selected = Array.IndexOf(mItems, thisObject);
        if (selected != -1) {
            switch (mItemsCollected) {
                case 0:
                    //Start the level timer only after the first object is picked up
                    StartLevelTimer();
                    break;
                case 1:
                    ChargeBattery();
                    break;
                case 2:
                    //the battery is still charging, so tell the user to wait
                    GameManager.Instance.ShowMissionInfo();
                    return;
            }
            thisObject.GetComponent<SimpleMissionItem>().SetCollected(true);
            thisObject.SetActive(false);
            if (++mItemsCollected == TOTALITEMS) {
                mActualCurrentTime = mCurrentTime;
                MissionFinished();
                return;
            }
            mItems[mItemsCollected].SetActive(true);
            UpdateUI();
        }
    }

    private void ChargeBattery() {
        StartCoroutine(Charge());
    }

    private IEnumerator Charge() {
        mCanvas.SetActive(true);
        EnergyBar bar = mCanvas.transform.FindChild("ChargingBar").GetComponent<EnergyBar>();
        int counter = 0;
        while(counter != 20) {
            yield return new WaitForSeconds(1);
            ++counter;
            bar.valueCurrent = counter;
        }
        mItems[mItemsCollected].GetComponent<SimpleMissionItem>().SetCollected(true);
        mItems[mItemsCollected].SetActive(false);
        ++mItemsCollected;
        mItems[mItemsCollected].SetActive(true);
        UpdateUI();
    }

    public void OnExit(GameObject thisObject, Collider other) {
    }

    public void OnStay(GameObject thisObject, Collider other) {
    }

    void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Transform missionItemsTransform = transform.FindChild("MissionItems");
        for (int i = 0; i < (missionItemsTransform.childCount - 1); ++i) {
            Gizmos.DrawLine(missionItemsTransform.GetChild(i).transform.position, missionItemsTransform.GetChild(i + 1).transform.position);
        }
    }
}
