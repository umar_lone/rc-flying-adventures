﻿using UnityEngine;
using System.Collections;
using System;

public class Level12Manager : LevelManagerBase, OnTriggerListener {
    private GameObject[] mItems;
    private int mItemsCollected = 0;
    private int TOTALITEMS;
    private GameObject mSightCone;

    public override void OnStart() {
        mSightCone = transform.FindChild("DumbObjects/SightCone").gameObject;
        Transform missionItemsTransform = transform.FindChild("MissionItems");
        TOTALITEMS = missionItemsTransform.childCount;
        mItems = new GameObject[TOTALITEMS];
        for (int i = 0; i < TOTALITEMS; ++i) {
            mItems[i] = missionItemsTransform.GetChild(i).gameObject;
            SimpleMissionItem item = mItems [i].GetComponent<SimpleMissionItem>();
            item.SetOnTriggerListener(this);
            mItems[i].SetActive(false);
        }
        Array.Sort(mItems, (o1, o2) => {
            return o1.name.CompareTo(o2.name);
        });
        mItems[0].SetActive(true);

        UpdateUI();
    }
    public override string GetMissionMessage() {
        return mItems[mItemsCollected].GetComponent<SimpleMissionItem>().GetObjectiveMessage();
    }
    private void UpdateUI() {
        UIManager.Instance.SetObjectiveItem(mItems[mItemsCollected]);
        GameManager.Instance.ShowMissionInfo();
    }

    private void MissionFinished() {
        mMissionFinished = true;
        GameManager.Instance.MissionFinished("");

    }
    public void OnEnter(GameObject thisObject, Collider other) {
        if (other.tag != "Player") {
            return;
        }
        int selected = Array.IndexOf(mItems, thisObject);
        if (selected != -1) {
            switch (mItemsCollected) {
                case 0:
                    //Start the level timer only after the first object is picked up
                    StartLevelTimer();
                    break;
            }
            thisObject.GetComponent<SimpleMissionItem>().SetCollected(true);
            thisObject.SetActive(false);
            if (++mItemsCollected == TOTALITEMS) {
                mActualCurrentTime = mCurrentTime;
                MissionFinished();
                return;
            }
            mItems[mItemsCollected].SetActive(true);
            UpdateUI();
        }
    }

    public void OnExit(GameObject thisObject, Collider other) {
    }

    public void OnStay(GameObject thisObject, Collider other) {
    }

    void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Transform missionItemsTransform = transform.FindChild("MissionItems");
        for (int i = 0; i < (missionItemsTransform.childCount - 1); ++i) {
            Gizmos.DrawLine(missionItemsTransform.GetChild(i).transform.position, missionItemsTransform.GetChild(i + 1).transform.position);
        }
    }
}