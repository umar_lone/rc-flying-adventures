﻿using UnityEngine;
using System.Collections;

public class SightCone : MonoBehaviour {
    public float speed = 1;
    private bool mRight = true;
    private bool mSweep = true;
    public LayerMask layer;
    public int raycastLength = 10;
    public GameObject warningUI;
    private GameObject mWarningUI;
    //public GameObject DebugPlayer;
    // Use this for initialization
    void Start() {

    }

    void Update() {
        if (mSweep) {
            if (mRight == true) {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 60, 0), Time.deltaTime * speed);
                if (transform.rotation.eulerAngles.y > 59f) {
                    mRight = false;
                }
            }
            else {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 0, 0), Time.deltaTime * speed);
                if (transform.rotation.eulerAngles.y < 1f) {
                    mRight = true;
                }
            }
        }
        else {
            Vector3 playerPosition = GameManager.Instance.GetCurrentVehicle().transform.position;
            playerPosition.y = transform.position.y;
            transform.LookAt(playerPosition);
        }
        
    }

    public void OnTriggerExit(Collider other) {
        if (other.tag == "Player") {
            if (mWarningUI != null)
                GameObject.Destroy(mWarningUI);
            mSweep = true;
            mWarningUI = null;

        }
    }

    public void OnTriggerStay(Collider other) {
        if (other.tag == "Player") {
            RaycastHit hit;
            Physics.Linecast(transform.position, GameManager.Instance.GetCurrentVehicle().transform.position, out hit, layer);
           
            if (hit.transform != null && hit.transform.gameObject.tag == "Player") {
                if (mWarningUI == null) {
                    mWarningUI = Instantiate(warningUI);
                    UIGenericWarning warning = mWarningUI.GetComponent<UIGenericWarning>();
                    warning.SetWarningMessage("The owner has seen you");
                    warning.SetWarningTime(5);
                    warning.SetFailureMessage("The owner made a delicious omlette of your precious egg");
                }
                mSweep = false;
            }
            /*
            If the player is in view and then hides from sight (but within sightcone),
            then we need to close warning dialog and resume sweeping
            */
            else if(mSweep == false) {
                if (mWarningUI != null)
                    GameObject.Destroy(mWarningUI);
                mSweep = true;
                mWarningUI = null;
            }
        }
    }
    void OnDrawGizmos() {
        Gizmos.color = Color.gray;
        if(mSweep)
            Gizmos.DrawLine(transform.position, GameManager.Instance.GetCurrentVehicle().transform.position);
    }
}
