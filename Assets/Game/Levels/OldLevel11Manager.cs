﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

public class OldLevel11Manager : LevelManagerBase, OnTriggerListener
{
    private GameObject[] mItems;
    private int mItemsCollected = 0;
    private int TOTALITEMS;
    private EnergyBar progressBar;
    private int mTotalLeaves;

    // Use this for initialization
    public override void OnStart()
    {
        Transform missionItemsTransform = transform.FindChild("MissionItems");
        TOTALITEMS = missionItemsTransform.childCount;
        mItems = new GameObject[TOTALITEMS];
        for (int i = 0; i < TOTALITEMS; ++i)
        {
            mItems[i] = missionItemsTransform.GetChild(i).gameObject;
            SimpleMissionItem item = mItems[i].GetComponent<SimpleMissionItem>();
            item.SetOnTriggerListener(this);
            mItems[i].SetActive(false);
        }
        Array.Sort(mItems, (o1, o2) => {
            return o1.name.CompareTo(o2.name);
        });
        mItems[0].SetActive(true);
        UpdateUI();
        InitializeProgressBar();
    }
    public override string GetMissionMessage()
    {
        return mItems[mItemsCollected].GetComponent<SimpleMissionItem>().GetObjectiveMessage();
    }
    private void UpdateUI()
    {
        UIManager.Instance.SetObjectiveItem(mItems[mItemsCollected]);
        GameManager.Instance.ShowMissionInfo();
        //UIManager.Instance.SetMissionMessage(mItems [mItemsCollected].GetComponent<SimpleMissionItem>().GetObjectiveMessage());
    }

    private void MissionFinished()
    {
        mMissionFinished = true;
        GameManager.Instance.MissionFinished("");

    }
    public void OnEnter(GameObject thisObject, Collider other)
    {
        if (other.tag != "Player")
        {
            return;
        }
        int selected = Array.IndexOf(mItems, thisObject);
        if (selected != -1)
        {
            thisObject.GetComponent<SimpleMissionItem>().SetCollected(true);
            thisObject.SetActive(false);
            if (mItemsCollected == 0)
            {
                //Start the level timer only after the first ring is picked up
                StartLevelTimer();
            }
            if (++mItemsCollected == TOTALITEMS)
            {
                mActualCurrentTime = mCurrentTime;
                MissionFinished();
                return;
            }
            mItems[mItemsCollected].SetActive(true);
            UpdateUI();
        }
    }

    public void OnExit(GameObject thisObject, Collider other)
    {
    }

    public void OnStay(GameObject thisObject, Collider other)
    {
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Transform missionItemsTransform = transform.FindChild("MissionItems");
        for (int i = 0; i < (missionItemsTransform.childCount - 1); ++i)
        {
            Gizmos.DrawLine(missionItemsTransform.GetChild(i).transform.position, missionItemsTransform.GetChild(i + 1).transform.position);
        }
    }

    private void InitializeProgressBar()
    {
        progressBar = transform.FindChild("Canvas/Filled Bar").gameObject.GetComponent<EnergyBar>();
        GameObject item3_alternative = transform.FindChild("MissionItems/Item3_MissionClearLeaves").gameObject; ;
        for (int i = 0; i < item3_alternative.transform.childCount; ++i)
        {
            if (item3_alternative.transform.GetChild(i).gameObject.activeSelf)
            {
                ++mTotalLeaves;
            }
        }
        //Let the user clear half the leaves
        progressBar.SetValueMax(mTotalLeaves / 2);
        progressBar.valueCurrent = 0;
        progressBar.gameObject.SetActive(false);
    }

    public void SetProgressBarVisibility(bool visible)
    {
        progressBar.gameObject.SetActive(visible);
    }
}
