﻿using UnityEngine;
using System.Collections;

public class AmbientSound : MonoBehaviour {
    public bool playAtNight = false;
    private AudioSource mAudioSource;
    private bool mShouldPlay = false;
	// Use this for initialization
	void Start () {
        mAudioSource = GetComponent<AudioSource>();
        GameManager.Instance.RegisterAudioSource(mAudioSource);
        mAudioSource.volume = GameManager.Instance.GetSoundSetting();
        mAudioSource.loop = true;
        mAudioSource.playOnAwake = false;
        mAudioSource.Stop();
        GameObject dayController = GameObject.Find("DayNightController");
        DayNightController controller = dayController.GetComponent<DayNightController>();
        controller.dayTimeChanged += OnDayTimeChanged;

        controller.NotifyTimeListeners();
	}
    void PlaySound() {
        //Nothing to do if the audiosource is disabled
        if (mAudioSource.enabled == false)
            return;
        if (mShouldPlay) {
            if(!mAudioSource.isPlaying)
                mAudioSource.Play();
        } else {
            if(mAudioSource.isPlaying)
                mAudioSource.Stop();
        }
    }
	void OnTriggerEnter(Collider player) {
        if(player.tag == "Player") {
            mShouldPlay = true;
            PlaySound();
        }
    }
    void OnTriggerExit(Collider player) {
        if (player.tag == "Player") {
            mShouldPlay = false;
            PlaySound();
        }
    }

    private void OnDayTimeChanged(DayNightController.TimeOfDay time) {
        switch (time) {
            case DayNightController.TimeOfDay.DAY:
                mAudioSource.enabled = true;
                break;
            case DayNightController.TimeOfDay.NIGHT:
            case DayNightController.TimeOfDay.DUSK:
            case DayNightController.TimeOfDay.DAWN:
                mAudioSource.enabled = playAtNight;
                break;
            default:
                break;
        }
    }
}
