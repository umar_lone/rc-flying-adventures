﻿using UnityEngine;
using System.Collections;

public class BlinkingLight : MonoBehaviour {
    public float blinkSpeed = 1.0f;
    public float lightOnIntensity =1.0f;
    public float lightOffIntensity= 0.0f;
    public Color lightColor;
    private float mSecondsElapsed  = 0.0f;
    private Light mLight;
    // Use this for initialization
    void Start() {
        mLight = GetComponent<Light>();
        mLight.intensity = lightOnIntensity;
        mLight.color = lightColor;
    }

    // Update is called once per frame
    void Update() {
        mSecondsElapsed += Time.deltaTime;
        if (mLight.intensity == lightOnIntensity && mSecondsElapsed >= blinkSpeed) {
            mLight.intensity = lightOffIntensity;
            mSecondsElapsed = 0.0f;
        } else if (mLight.intensity == lightOffIntensity && mSecondsElapsed >= blinkSpeed) {
            mLight.intensity = lightOnIntensity;
            mSecondsElapsed = 0.0f;
        }
    }

}
