﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class DayNightController : MonoBehaviour {
    //Debug Stuff

    [Range(0, 23)]
    public int DebugTime;
    [Range(0, 1)]
    public float ambientLight;
    public Color skyColor;

    public delegate void OnLightChanged(TimeOfDay time);
    public event OnLightChanged dayTimeChanged;

    public GameObject mainLight;
    public Material skyDay;
    public Material skyTwilight;
    public Material skySunset;
    public Material skyNight;
    public enum TimeOfDayOption { AUTOMATIC, AUTOMATICFAST, DAY, NIGHT };
    public TimeOfDayOption userTimeOption;

    public enum TimeOfDay { NIGHT, DAY, DUSK, DAWN };
    private TimeOfDay mCurrentTimeType;
    private int mCurrentHour;
    private const float TIMEMULTIPLIER = 7.5F;


    private readonly Color MAINLIGHTCOLOR_ORANGE = new Color(.6f, .33f, 0f);
    private readonly Color MAINLIGHT_NIGHT_COLOR_BLUE = new Color(.6f, .6f, 1F);
    private readonly Color MAINLIGHTCOLOR_GREY = new Color(.4f, .4f, .4f);
    private readonly Color FOG_DAWNCOLOR_DARKGREY = new Color(.18f, .18f, .18f);
    private readonly Color FOG_DAYCOLOR_LIGHTGREY = new Color(.83f, .83f, .83f);
    private Coroutine mDayNightCycle;
    private Coroutine mDayNightCycle4x;
    // Use this for initialization
    void Start() {
        SetTimeOfDayOption();
    }

    public void NotifyTimeListeners() {
        SetTimeOfDay();
    }

    private IEnumerator DayNightCycle() {
        while ((userTimeOption == TimeOfDayOption.AUTOMATIC)) {
            switch (userTimeOption) {
                case TimeOfDayOption.AUTOMATIC:
                    mCurrentHour = DateTime.Now.Hour;
                    //mCurrentHour = DebugTime; //Used for debugging
                    break;
                case TimeOfDayOption.AUTOMATICFAST:
                    break;
                case TimeOfDayOption.DAY:
                    mCurrentHour = 12;
                    break;
                case TimeOfDayOption.NIGHT:
                    mCurrentHour = 0;
                    break;
            }
            SetTimeOfDay();
            //yield return new WaitForSeconds(2);//Used during debugging
            yield return new WaitForSeconds(30);
        }
        yield return new WaitForSeconds(1f);
    }
    private IEnumerator DayNightFast() {
        int time4x = mCurrentHour;
        while (userTimeOption == TimeOfDayOption.AUTOMATICFAST) {
            time4x++;
            if (time4x > 23) {
                time4x = 0;
            }
            mCurrentHour = time4x;
            SetTimeOfDay();
            //Will cause time to change in 24 minutes
            yield return new WaitForSeconds(60);
        }
    }

    private void SetTimeOfDay() {
        //Rotation just like sun
        mainLight.transform.rotation = Quaternion.Euler(new Vector3(mCurrentHour * TIMEMULTIPLIER, 180, 0));

        //Night
        if ((mCurrentHour >= 0 && mCurrentHour <= 4) || (mCurrentHour >= 21 && mCurrentHour <= 23)) {
            mainLight.GetComponent<Light>().intensity = 0;
            RenderSettings.ambientIntensity = .25f;
            RenderSettings.ambientLight = MAINLIGHT_NIGHT_COLOR_BLUE;
            RenderSettings.skybox = skyNight;
            RenderSettings.fog = false;
            if (dayTimeChanged != null)
                dayTimeChanged(TimeOfDay.NIGHT);
        }
        //Dawn
        if (mCurrentHour >= 5 && mCurrentHour <= 8) {
            float difference = 8 - mCurrentHour;
            //3 is 8-5
            float factor = 3 - difference;
            float intensity = .25f + (float)factor / 3;
            mainLight.GetComponent<Light>().intensity = intensity;
            RenderSettings.ambientIntensity = intensity;
            RenderSettings.ambientLight = MAINLIGHTCOLOR_ORANGE;
            RenderSettings.skybox = skyTwilight;
            RenderSettings.fog = true;
            RenderSettings.fogColor = FOG_DAWNCOLOR_DARKGREY;
            RenderSettings.fogDensity = 0.0005f;
            if (dayTimeChanged != null)
                dayTimeChanged(TimeOfDay.DAWN);
        }
        //Dusk
        if (mCurrentHour >= 17 && mCurrentHour <= 20) {
            float difference = 20 - mCurrentHour;
            //3 is 20-17
            float factor = 3 - difference;
            float intensity = 1 - factor / 3;
            mainLight.GetComponent<Light>().intensity = intensity;
            //Small adjustment for turning down ambient light intensity at 6pm, otherwise it is too bright
            if((int)intensity == 1) {
                intensity = 0.7f;
            }
            //Intensity goes below 0.25f, so don't allow that (as night uses .25f intensity)
            if (intensity < .2f)
                intensity = .25f;
            RenderSettings.ambientIntensity = intensity;
            RenderSettings.ambientLight = MAINLIGHTCOLOR_ORANGE;
            RenderSettings.skybox = skyTwilight;
            RenderSettings.fog = true;
            RenderSettings.fogColor = FOG_DAWNCOLOR_DARKGREY;
            RenderSettings.fogDensity = 0.0005f;
            if (dayTimeChanged != null)
                dayTimeChanged(TimeOfDay.DUSK);
        }
        //Day
        if (mCurrentHour >= 8 && mCurrentHour <= 16) {
            mainLight.GetComponent<Light>().intensity = 1f;
            RenderSettings.ambientIntensity = 1f;
            RenderSettings.ambientLight = MAINLIGHTCOLOR_GREY;
            RenderSettings.skybox = skyDay;
            RenderSettings.fog = true;
            RenderSettings.fogColor = FOG_DAYCOLOR_LIGHTGREY;
            RenderSettings.fogDensity = 0.0005f;
            if (dayTimeChanged != null)
                dayTimeChanged(TimeOfDay.DAY);


        }
        ambientLight = RenderSettings.ambientIntensity;
    }

    public void SetTimeOfDayOption() {
        switch (GameManager.Instance.GetTimeOfDaySetting()) {
            case 0:
                userTimeOption = TimeOfDayOption.AUTOMATIC;
                break;
            case 1:
                userTimeOption = TimeOfDayOption.AUTOMATICFAST;
                break;
            case 2:
                userTimeOption = TimeOfDayOption.DAY;
                mCurrentHour = 13; break;
            case 3:
                userTimeOption = TimeOfDayOption.NIGHT;
                mCurrentHour = 0; break;
            default:
                break;
        }
        switch (userTimeOption) {
            case TimeOfDayOption.AUTOMATIC:
                if (mDayNightCycle == null)
                    mDayNightCycle = StartCoroutine(DayNightCycle());
                break;
            case TimeOfDayOption.AUTOMATICFAST:
                if (mDayNightCycle4x == null)
                    mDayNightCycle4x = StartCoroutine(DayNightFast());
                break;
            case TimeOfDayOption.DAY:
                if (mDayNightCycle != null) {
                    StopCoroutine(mDayNightCycle);
                    mDayNightCycle = null;
                }
                if (mDayNightCycle4x != null) {
                    StopCoroutine(mDayNightCycle4x);
                    mDayNightCycle4x = null;
                }
                SetTimeOfDay();
                break;
            case TimeOfDayOption.NIGHT:
                if (mDayNightCycle != null) {
                    StopCoroutine(mDayNightCycle);
                    mDayNightCycle = null;
                }
                if (mDayNightCycle4x != null) {
                    StopCoroutine(mDayNightCycle4x);
                    mDayNightCycle4x = null;
                }
                SetTimeOfDay();
                break;
            default:
                break;
        }
    }
    public void OnTimeChanged() {
        //if (toggleauto.isOn) {
        //    userTimeOption = TimeOfDayOption.AUTOMATIC;
        //}else if (toggleauto4x.isOn) {
        //    userTimeOption = TimeOfDayOption.AUTOMATICFAST;
        //}
        //else if(toggleday.isOn) {
        //    userTimeOption = TimeOfDayOption.DAY;
        //    mCurrentHour = 13;
        //}else if (togglenight.isOn) {
        //    userTimeOption = TimeOfDayOption.NIGHT;
        //    mCurrentHour = 0;
        //}
        //SetTimeOfDay(userTimeOption);
    }
}
