﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GamePauseButtonHandler : MonoBehaviour {
    public GameObject pauseUI;
    public GameObject helpUI;
    public GameObject missionCameraPrefab;
	// Use this for initialization
	void Start () {
	}

    public void OnPause() {
        //GameManager.Instance.SetTimeScale(0f);
        Instantiate(pauseUI);
    }
    public void OnShowMissionInfo() {
        GameManager.Instance.ShowMissionInfo();
    }
    //Used for debugging only
    public void OnDebug() {
        Instantiate(missionCameraPrefab);
    }
    public void OnHelp() {
        Instantiate(helpUI);
    }
}
