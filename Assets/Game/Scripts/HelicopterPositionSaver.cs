﻿using UnityEngine;
using System.Collections;
using System;

struct VehicleState {
    public Vector3 mPosition;
    public Quaternion mRotation;
    public float mEnginePower;
}
public class HelicopterPositionSaver : MonoBehaviour {
    public int positionsToSave;
    public float interval;
    private VehicleState[] mVehicleState;
    private float mCounter;
    private GameObject mCurrentVehicle;
    private int mCurrentPosition;
    // Use this for initialization
    void Start () {
        mCurrentVehicle = GameManager.Instance.GetCurrentVehicle();
        mVehicleState = new VehicleState[positionsToSave];
        mCounter = 0f;
        StartCoroutine(SavePositions());
	}

    private IEnumerator SavePositions() {
        mCurrentPosition = 0;
        while (true) {
            mVehicleState[mCurrentPosition].mPosition = mCurrentVehicle.transform.position;
            mVehicleState[mCurrentPosition].mRotation = mCurrentVehicle.transform.rotation;
            mVehicleState[mCurrentPosition].mEnginePower = mCurrentVehicle.GetComponent<HelicopterController>().EngineForce;
            yield return new WaitForSeconds(interval) ;
            if(++mCurrentPosition == positionsToSave) {
                mCurrentPosition = 0;
            }
            
        }
        throw new NotImplementedException();
    }
    public void OnReverse() {
        if(mCurrentPosition == 0) {
            if (mVehicleState[mVehicleState.Length - 1].mPosition == Vector3.zero) {
                print("No more saves");
                return;
            }
            else {
                mCurrentPosition = mVehicleState.Length - 1;
            }
        }
        --mCurrentPosition;
        mCurrentVehicle.transform.position = mVehicleState[mCurrentPosition].mPosition;
        mCurrentVehicle.transform.rotation = mVehicleState[mCurrentPosition].mRotation;
        mCurrentVehicle.GetComponent<HelicopterController>().EngineForce = mVehicleState[mCurrentPosition].mEnginePower;
    }

}
