﻿using UnityEngine;
using System.Collections;

public class LevelBounds : MonoBehaviour {
    public GameObject warningUI;
    private GameObject warningUIInstance;
    private bool mWarnPlayer = false;
	// Use this for initialization

    /*
    Triggers get called multiple times due to multiple colliders on
    the helicopter. So,we do a check on warningUIInstance before
    doing anything in the trigger methods
    */
    void OnTriggerEnter(Collider other) {
        //WarningUIInstance check is required. Read the above comment
        if (other.tag == "Player" && warningUIInstance == null) {
            warningUIInstance = Instantiate(warningUI);
        }
    }
    void OnTriggerExit(Collider other) {
        //WarningUIInstance check is required. Read the above comment
        if (other.tag == "Player" && warningUIInstance != null) {
            warningUIInstance.GetComponent<UIWarning>().StopWarning();
            warningUIInstance = null;
        }

    }
}
