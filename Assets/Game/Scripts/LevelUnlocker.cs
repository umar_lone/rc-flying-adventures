﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class LevelUnlocker : MonoBehaviour {
    private GameObject[] mLevels;
	// Use this for initialization
	void Start () {
        GameObject levelParent = transform.FindChild("Canvas/Horizontal Scroll Snap/Content").transform.gameObject;
        int childCount = levelParent.transform.childCount;
        mLevels = new GameObject[childCount];

        for(int i = 0; i < childCount; i++) {
            mLevels[i] = levelParent.transform.GetChild(i).gameObject;
            mLevels[i].transform.FindChild("Lock").gameObject.SetActive(true);
        }
        Array.Sort(mLevels, (o1, o2) => {
            return o1.name.CompareTo(o2.name);
        });
        int currentScene = GameManager.Instance.GetCurrentSceneIndex();
        //Assign best times
        for (int i = 0; i < GameManager.Instance.GetMaxLevelsInScene(currentScene); ++i) {
            Text textTime = mLevels[i].transform.FindChild("Text-Time").GetComponent<Text>();
            float seconds = GameManager.Instance.GetSceneBestTimes(currentScene)[i];
            //Either the level has not been played yet or it is the first level
            if (seconds == -1)
                continue;
            TimeSpan time = TimeSpan.FromSeconds(seconds);
            if (time.Seconds < 10) {
                //This is only to show 0 before second when it is less than 10
                string t = string.Format("0{0}", time.Seconds);
                textTime.text = String.Format("Best time [{0}:{1}]", time.Minutes, t);
            } else {
                textTime.text = String.Format("Best time [{0}:{1}]", time.Minutes, time.Seconds);
            }
        }
        //First two levels are always unlocked, so start from 3rd level
        for(int i = 0; i < GameManager.Instance.GetLevelsUnlockedForCurrentScene(); ++i) {
            UnlockLevel(i);
        }
	}
    private void UnlockLevel(int level) {
        mLevels[level].transform.FindChild("Lock").gameObject.SetActive(false);
    }
    //public void LockLevel(int level) {
    //    mLevels[level].transform.FindChild("Lock").gameObject.SetActive(true);
    //}

}
