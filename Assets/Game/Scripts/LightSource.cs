﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Attach this script to a gameobject that has the following hierarchy
/// MainGameObject
/// |-LightSource
///     |-Emmissive
///         |Put all emmissive lights here
///     |-Light
///         |All objects that have Light component
/// </summary>
public class LightSource : MonoBehaviour {
    private Light []mLights;
    private GameObject mEmmissiveObject;
    private Behaviour []mHalos;
    private bool mHasEmissiveObjects = false;
	// Use this for initialization
	void Start () {
        
        GameObject dayController = GameObject.Find("DayNightController");
        DayNightController controller = dayController.GetComponent<DayNightController>();
        controller.dayTimeChanged += OnLightChanged;

        mEmmissiveObject = transform.FindChild("Emmissive").gameObject ;
        if(mEmmissiveObject.transform.childCount > 0) {
            mHasEmissiveObjects = true;
        }
        GameObject lightsParent = transform.FindChild("Normal").transform.gameObject;
        mLights = lightsParent.GetComponentsInChildren<Light>();
        if(mLights == null || mLights.Length == 0) {
            throw new System.Exception("No lights attached to object");
        }
        int totalLights = mLights.Length;
        mHalos = new Behaviour[totalLights];
        GameObject[] lightObjects = new GameObject[totalLights];
 
        for (int i = 0; i < totalLights; ++i) {
            GameObject light = lightsParent.transform.GetChild(i).gameObject;
            mHalos[i] = light.GetComponent("Halo") as Behaviour;
        }
        controller.NotifyTimeListeners();
    }
    public void SetLightState(bool state) {
        if (mHasEmissiveObjects) {
            mEmmissiveObject.SetActive(state);
        }
        if (mLights != null) {
            for (int i = 0; i < mLights.Length; ++i) {
                mLights[i].enabled = state;
                if(mHalos[i] != null)
                    mHalos[i].enabled = state;
            }
        }
    }
    private void OnLightChanged(DayNightController.TimeOfDay time) {
        switch (time) {
            case DayNightController.TimeOfDay.DAY:
                SetLightState(false);
                break;
            case DayNightController.TimeOfDay.NIGHT:
            case DayNightController.TimeOfDay.DUSK:
            case DayNightController.TimeOfDay.DAWN:
                SetLightState(true);
                break;
            default:
                break;
        }
    }

    

}
