﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour {
    public EnergyBar loadingBar;
    public Text loadingText;
    private Text mTextTips;
    public string [] tips;
    private string mCurrentTip;
    private AsyncOperation mAO;
	// Use this for initialization
	void Start () {
        //First scene is Menu
        mAO = SceneManager.LoadSceneAsync(GameManager.Instance.GetCurrentSceneIndex()+1);
        mTextTips = transform.FindChild("Canvas/Panel-Main/Text-MessageTips").gameObject.GetComponent<Text>();
        mTextTips.text = tips[Random.Range(0, tips.Length)];
	}
	
	// Update is called once per frame
	void Update () {
        var progress= (int) (mAO.progress * 100);
        loadingBar.valueCurrent = progress;
	}
}
