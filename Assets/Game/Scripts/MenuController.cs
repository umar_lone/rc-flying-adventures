﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.UI.Extensions;
using System.Collections.Generic;

//Dummy comment for commit
public class MenuController : MonoBehaviour {
    [Header("Vehicles Used In Levels")]
    public VehicleType[] levelVehicles;
    public GameObject optionsUI;
    public GameObject dialog;
    public GameObject aboutDialog;
    public GameObject simpleMessage;
    private GameObject mMainMenu;
    private GameObject mEpisodeSelector;
    private GameObject mLevelSelector;
    private GameObject mVehicleSelector;
    private Dictionary<int, VehicleType> mLevelVehicles = new Dictionary<int, VehicleType>();
    private Toggle toggleSimpleHeli;
    private Toggle toggleAttackHeli;
    private AudioSource mAudioMusic;

    private Text mTextVehicleMsg;

    // Use this for initialization
    void Start() {
        for(int i = 0; i < levelVehicles.Length; ++i) {
            mLevelVehicles.Add(i, levelVehicles[i]);
        }
        //Required because in other scenes menu controller won't be available
        GameManager.Instance.SetLevelVehicles(mLevelVehicles);
        mAudioMusic = GetComponent<AudioSource>();
        mAudioMusic.volume = GameManager.Instance.GetMusicSetting();
        mAudioMusic.Play();
        mAudioMusic.loop = true;
        GameManager.Instance.SetCurrentMusicSource(mAudioMusic);

        GameObject menu = GameObject.Find("Menus");

        mMainMenu = menu.transform.FindChild("MainMenu").gameObject;
        mMainMenu.SetActive(true);
        
        mEpisodeSelector = menu.transform.FindChild("EpisodeSelector").gameObject;
        MakeTransparent(mEpisodeSelector);
        mEpisodeSelector.SetActive(false);

        mLevelSelector = menu.transform.FindChild("LevelSelector").gameObject;
        MakeTransparent(mLevelSelector);
        mLevelSelector.SetActive(false);

        mVehicleSelector = menu.transform.FindChild("VehicleSelector").gameObject;
        MakeTransparent(mVehicleSelector);
        mVehicleSelector.SetActive(false);

        mTextVehicleMsg = menu.transform.FindChild("VehicleSelector/Canvas/Panel/Text-VehicleMsg").GetComponent<Text>();
        toggleSimpleHeli = menu.transform.FindChild("VehicleSelector/Canvas/Panel/ToggleSimpleHeli").GetComponent<Toggle>();
        toggleAttackHeli = menu.transform.FindChild("VehicleSelector/Canvas/Panel/ToggleAttackHeli").GetComponent<Toggle>();

    }
    private void MakeTransparent(GameObject go) {
        go.transform.FindChild("Canvas").gameObject.GetComponent<CanvasGroup>().alpha = 0f;
    }

    void FadeInOut(GameObject outgoing, GameObject incoming) {
        StartCoroutine(Fade(outgoing, incoming));
    }

    private IEnumerator Fade(GameObject outgoing, GameObject incoming) {
        CanvasGroup fadeout = outgoing.GetComponent<CanvasGroup>();
        fadeout.interactable = false;

        while (fadeout.alpha > 0) {
            fadeout.alpha -= Time.deltaTime *4;
            yield return null;
        }
        outgoing.transform.parent.gameObject.SetActive(false);

        incoming.transform.parent.gameObject.SetActive(true);
        CanvasGroup fadein = incoming.GetComponent<CanvasGroup>();
        while (fadein.alpha < 1) {
            fadein.alpha += Time.deltaTime * 4;
            yield return null;
        }
        fadein.interactable = true;
        yield return null;
    }

    #region MAINMENU HANDLERS
    public void OnPlay() {
        FadeInOut(mMainMenu.transform.FindChild("Canvas").gameObject,
            mEpisodeSelector.transform.FindChild("Canvas").gameObject);

    }
    public void OnOptions() {
        Instantiate(optionsUI);

    }
    public void OnExit() {
        GameObject messageBox = Instantiate(dialog);
        UIMessageDialog dlg = messageBox.GetComponent<UIMessageDialog>();
        dlg.SetMessage("Are you sure you want to quit?");
        dlg.onNoClicked += () => { };
        dlg.onYesClicked += () => { Application.Quit(); };
        
    }
    #endregion

    #region EPISODE HANDLERS
    public void OnEpisodeBack() {
        FadeInOut(mEpisodeSelector.transform.FindChild("Canvas").gameObject, 
            mMainMenu.transform.FindChild("Canvas").gameObject
           );
    }
    public void OnEpisodeNext() {
        HorizontalScrollSnap episodes = mEpisodeSelector.transform.FindChild("Canvas/Horizontal Scroll Snap").GetComponent<HorizontalScrollSnap>();
        if (episodes.CurrentScreen() == 1) {
            GameObject go =Instantiate(simpleMessage);
            go.GetComponent<UIMissionInfo>().SetTitle("Alert");
            go.GetComponent<UIMissionInfo>().SetMissionText("Not available yet. Please check back shortly");
            return;
        }

        GameManager.Instance.SetCurrentSceneIndex(episodes.CurrentScreen());
     
        FadeInOut(mEpisodeSelector.transform.FindChild("Canvas").gameObject,
            mLevelSelector.transform.FindChild("Canvas").gameObject);
    }
    #endregion


    #region LEVEL HANDLERS
    public void OnLevelBack() {
        FadeInOut(mLevelSelector.transform.FindChild("Canvas").gameObject,
            mEpisodeSelector.transform.FindChild("Canvas").gameObject);
        //levelSelector.SetActive(false);
        //episodeSelector.SetActive(true);
    }
    public void OnLevelNext() {
        HorizontalScrollSnap levels = mLevelSelector.transform.FindChild("Canvas/Horizontal Scroll Snap").GetComponent<HorizontalScrollSnap>();
        int currentLevelIndex = levels.CurrentScreen();
        if(currentLevelIndex >= GameManager.Instance.GetLevelsUnlockedForCurrentScene()) {
            GameObject go =Instantiate(simpleMessage);
            go.GetComponent<UIMissionInfo>().SetTitle("Alert");
            go.GetComponent<UIMissionInfo>().SetMissionText("Unlock by completing the previous levels first");
            return ;
        }
        GameManager.Instance.SetCurrentLevelIndex(levels.CurrentScreen());
        FadeInOut(mLevelSelector.transform.FindChild("Canvas").gameObject,
            mVehicleSelector.transform.FindChild("Canvas").gameObject);
        //episodeSelector.SetActive(false);
        //heliSelector.SetActive(true);
    }
    #endregion

    #region HELICOPTER SELECTION
    public void OnHeliMenuBack() {
        FadeInOut(mVehicleSelector.transform.FindChild("Canvas").gameObject,
            mLevelSelector.transform.FindChild("Canvas").gameObject);
        //heliSelector.SetActive(false);
        //levelSelector.SetActive(true);
        toggleSimpleHeli.isOn = true;
        mTextVehicleMsg.text = "";
    }
    public void OnHeliMenuForward() {
        //Set vehicle type here
        VehicleType type; 
        if (toggleSimpleHeli.isOn) {
            type = VehicleType.SIMPLEHELICOPTER;
        } else {
            type = VehicleType.ATTACKHELICOPTER;
        }
        int levelNo = GameManager.Instance.GetCurrentLevelIndex();
        if (mLevelVehicles.ContainsKey(levelNo)) {
            VehicleType vehicle = mLevelVehicles[levelNo];
            if(vehicle != VehicleType.ANY && vehicle != type) {
                mTextVehicleMsg.text = "This level requires " + vehicle;
                return;
            }
        }
        mTextVehicleMsg.text = "";
        GameManager.Instance.SetVehicleType(type);
        GameManager.Instance.LoadSelectedScene();
    }
    #endregion

    public void OnAbout() {
        GameObject go = Instantiate(aboutDialog);
        go.GetComponent<UIMessageDialog>().SetMessage("RC Adventures 1.0");
    }
    public void OnFacebook() {
        Application.OpenURL("https://www.facebook.com/poashtech/");
    }
}
