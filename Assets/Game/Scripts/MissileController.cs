﻿using UnityEngine;
using System.Collections;

public class MissileController : MonoBehaviour {
    Rigidbody rocket;
    public float force;
    // Use this for initialization
    void Start () {
        rocket = GetComponent<Rigidbody>();
        GameObject.Destroy(gameObject, 5);
        rocket.AddRelativeForce(Vector3.forward * force, ForceMode.Impulse);
    }
    void OnCollisionEnter(Collision other) {
    }
}
