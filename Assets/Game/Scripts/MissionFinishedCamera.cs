﻿using UnityEngine;
using System.Collections;

public class MissionFinishedCamera : MonoBehaviour {
    [Range(1f, 20f)]
    public float rotationSpeed = 1f;
    private GameObject mainCamera;
	// Use this for initialization
	void Start () {
        GameObject vehicleParent = GameManager.Instance.GetCurrentVehicle().transform.parent.gameObject;
        mainCamera = vehicleParent.transform.FindChild("FollowCamera/Main Camera").gameObject;
        mainCamera.SetActive(false);
        gameObject.transform.position = mainCamera.transform.position;
        gameObject.transform.rotation = mainCamera.transform.rotation;
        Vector3 originalPosition = gameObject.transform.position;

        gameObject.transform.position = originalPosition;

        gameObject.transform.LookAt(GameManager.Instance.GetCurrentVehicle().transform);
    }
}
