﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public static class CoroutineUtilities {
    public static IEnumerator WaitForRealTime(float delay) {
        while (true) {
            float pauseEndTime = Time.realtimeSinceStartup + delay;
            while (Time.realtimeSinceStartup < pauseEndTime) {
                yield return 0;
            }
            break;
        }
    }
}
//TODO Separate out the implementation of pause and mission failed
public class PauseController : MonoBehaviour {
    public GameObject optionsUI;
    public GameObject dialog;
    private float mOldTimeScale;
    private GameObject mWarningUI;
    private Text mTextMessage;
    private string mMessage;
  
    void Start() {
        mTextMessage = transform.FindChild("Canvas/Panel-Main/Text-Message").GetComponent<Text>();
        GameManager.Instance.PauseAudioSources();
        GameManager.Instance.SetCrossHairVisibility(false);
        /*
        When the pause menu comes up, the mission info dialog
        should be destroyed immediately to prevent it from
        overlapping the pause menu.
        */
        GameObject missionInfo = GameObject.FindGameObjectWithTag("UI_MISSIONINFO");
        if(missionInfo != null) {
            GameObject.Destroy(missionInfo);
        }

        /*
        Same thing for warning ui, but we only disable it and re-enable when
        user resumes.
        */
        mWarningUI = GameObject.FindGameObjectWithTag("UI_WARNING");
        if (mWarningUI != null) {
            UIWarning w = mWarningUI.GetComponent<UIWarning>();
            if(w != null) {
                w.PauseCountdown();
            }
            UIGenericWarning gw = mWarningUI.GetComponent<UIGenericWarning>();
            if(gw != null) {
                gw.PauseCountdown();
            }
        }

        mOldTimeScale = GameManager.Instance.GetTimeScale();
        GameManager.Instance.SetTimeScale(0f);
        MakeTransparent(gameObject);
        StartCoroutine(FadeIn(gameObject));
    }
    //Used by failed mission only
    public void SetFailureMessage(string message) {
        mMessage = message;
    }
    //This is used by Mission Failed menu only
    public void OnRestartWithoutConfirmation() {
        GameManager.Instance.RestartScene();
    }
    public void OnRestart() {
        GameObject messageBox = Instantiate(dialog);
        UIMessageDialog dlg = messageBox.GetComponent<UIMessageDialog>();
        dlg.SetMessage("You will lose any progress made in this level. Are you sure you want to restart?");
        dlg.onNoClicked += () => { };
        dlg.onYesClicked += () => { GameManager.Instance.RestartScene(); };
    }
    public void OnOptions() {
        Instantiate(optionsUI);
    }
    public void OnMenu() {
        GameObject messageBox = Instantiate(dialog);
        UIMessageDialog dlg = messageBox.GetComponent<UIMessageDialog>();
        dlg.SetMessage("You will lose any progress made in this level. Are you sure you want to go to the main menu?");
        dlg.onNoClicked += () => { };
        dlg.onYesClicked += () => { GameManager.Instance.ShowMainMenu(); };
    }
    public void OnResume() {
        StartCoroutine(FadeOut(gameObject));
        GameManager.Instance.SetTimeScale(mOldTimeScale);
        GameObject.Destroy(gameObject, 0.5f);
        /*
        Maybe user paused while the warning was being displayed,
        so set that warning active
        */
        if(mWarningUI != null)
            mWarningUI.GetComponent<UIWarning>().ResumeCountdown();
        GameManager.Instance.ResumeAudioSources();
        //Only for attack helicopter
        GameManager.Instance.SetCrossHairVisibility(true);

    }
    private void MakeTransparent(GameObject go) {
        go.transform.FindChild("Canvas").gameObject.GetComponent<CanvasGroup>().alpha = 0f;
    }
    
    private IEnumerator FadeIn(GameObject incoming) {
        incoming.SetActive(true);
        CanvasGroup fadein = incoming.transform.FindChild("Canvas").GetComponent<CanvasGroup>();
        //Used by failed mission only
        mTextMessage.text = mMessage;
        while (fadein.alpha < 1) {
            fadein.alpha += 0.1f;
            yield return CoroutineUtilities.WaitForRealTime(0.001f);
        }
        fadein.interactable = true;
        yield return null;
    }
    private IEnumerator FadeOut(GameObject outgoing) {
        CanvasGroup fadeout = outgoing.transform.FindChild("Canvas").GetComponent<CanvasGroup>();
        fadeout.interactable = false;

        while (fadeout.alpha > 0) {
            fadeout.alpha -= 0.1f;
            yield return CoroutineUtilities.WaitForRealTime(0.001f);

        }
        outgoing.SetActive(false);
        yield return null;
    }
}
