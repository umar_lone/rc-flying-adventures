﻿using UnityEngine;
using System.Collections;

public class SceneAudio : MonoBehaviour {
    private AudioSource mAudioMusic;
	// Use this for initialization
	void Start () {
        mAudioMusic = GetComponent<AudioSource>();
        mAudioMusic.volume = GameManager.Instance.GetMusicSetting();
        GameManager.Instance.SetCurrentMusicSource(mAudioMusic);
        mAudioMusic.Play();
        mAudioMusic.loop = true;
	}
}
