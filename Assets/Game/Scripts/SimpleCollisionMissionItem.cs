﻿using UnityEngine;
using System.Collections;
using System;

public class SimpleCollisionMissionItem : MonoBehaviour  {

    public string objectiveMessage;
    private OnCollisionListener mCollisionListener;
    private bool mCollected;
    public bool Collected() {
        return mCollected;
    }
    public string GetObjectiveMessage() {
        return objectiveMessage;
    }
    public void SetCollected(bool collected) {
        mCollected = collected;
    }
    public bool IsCollected() {
        return mCollected;
    }
    public void SetOnCollisionListener(OnCollisionListener listener) {
        mCollisionListener = listener;
    }
    void OnCollisionEnter(Collision other) {
        if (mCollisionListener != null)
            mCollisionListener.OnEnter(gameObject, other);
        else {
            print("mCollisionListener IS NULL FOR [" + gameObject.name + "]");
        }
    }

    void OnCollisionExit(Collision other) {
        if (mCollisionListener != null)
            mCollisionListener.OnExit(gameObject, other);
        else {
            print("mCollisionListener IS NULL FOR [" + gameObject.name + "]");
        }
    }

    void OnCollisionStay(Collision other) {
        if (mCollisionListener != null)
            mCollisionListener.OnStay(gameObject, other);
        else {
            print("mCollisionListener IS NULL FOR [" + gameObject.name + "]");
        }
    }
}
