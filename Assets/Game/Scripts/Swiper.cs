﻿using UnityEngine;
using System.Collections;

public class Swiper : MonoBehaviour {
    private Touch initTouch = new Touch();
    private float mRotationX = 0f;
    private float mRotationY = 0f;
    private Vector3 mInitialRotation;

    [Range(10, 180)]
    public float horizontalRotationAngle = 45;
    [Range(0, 180)]
    public float verticalRotationAngle = 45;

    [Range(0.1f, 1f)]
    public float dragSpeed = 0.2f;
    private float dir = -1;

    private Quaternion mOriginalRotationQtn;
    [Range(1f, 20f)]
    public float returnSpeed = 10f;
    private bool mRotating = false;

    private Vector3 mTempVector;
    private Vector3 mOrigRot;

    private float mSwipeStartTime;
    private Vector2 mStartPosition = new Vector2(0,0);
    private Vector3 mTouchPosition;

    // Use this for initialization
    void Start() {
        mTouchPosition = new Vector3(0, 0, 0);
        mTempVector = new Vector3(0, 0, 0);
        mOrigRot = transform.localEulerAngles;
        mInitialRotation = transform.localEulerAngles;
        mRotationX = mInitialRotation.x;
        mRotationY = mInitialRotation.y;

        mOriginalRotationQtn = transform.localRotation;
    }

    // Update is called once per frame
    void FixedUpdate() {

        if (Input.touchCount != 1)
            return;
        Touch touch = Input.GetTouch(0);
        mTouchPosition.x = touch.position.x;
        mTouchPosition.y = touch.position.y;
        Vector3 touchPosition = Camera.main.ScreenToViewportPoint(mTouchPosition);
        if (!((touchPosition.x >= 0.2f && touchPosition.x <= 0.8f) &&
            (touchPosition.y >= 0.3f && touchPosition.y <= 0.7f))) {
            return;
        }
        switch (touch.phase) {
            case TouchPhase.Began:
                initTouch = touch;
                mSwipeStartTime = Time.time;
                mStartPosition = new Vector2(touch.position.x, touch.position.y);
                mStartPosition.x = touch.position.x;
                mStartPosition.y =  touch.position.y;
                break;
            case TouchPhase.Moved:
                float deltaTime = Time.time - mSwipeStartTime;
                Vector2 endPosition = new Vector2(touch.position.x, touch.position.y);
                Vector2 swipeVector = endPosition-mStartPosition;
                float velocity = swipeVector.magnitude/deltaTime;
                if (velocity < 100)
                    return;
                float deltaX = initTouch.position.x - touch.position.x;
                float deltaY = initTouch.position.y - touch.position.y;
                if (Mathf.Abs(deltaX) > Mathf.Abs(deltaY)) {
                    //Horizontal swipe
                    mRotationX -= deltaY * Time.deltaTime * dragSpeed;
                    mRotationY += deltaX * Time.deltaTime * dragSpeed;
                    mRotationX = Mathf.Clamp(mRotationX, -verticalRotationAngle, verticalRotationAngle);
                    mRotationY = Mathf.Clamp(mRotationY, -horizontalRotationAngle, horizontalRotationAngle);
                    mTempVector.x = 17;
                    mTempVector.y = mRotationY;
                    transform.localEulerAngles = mTempVector;
                }
                break;
            case TouchPhase.Ended:
                //initTouch.position.x = 0;
                break;
        }
    }

    void Update() {
        if (mRotating) {
            transform.localRotation = Quaternion.Lerp(transform.localRotation,
                mOriginalRotationQtn,
                Time.deltaTime * returnSpeed);
        }
        float difference = Quaternion.Angle(transform.localRotation, mOriginalRotationQtn);
        if (difference == 0f) {
            mRotating = false;
        }
    }
    public void ResetCamera() {
        //cam.transform.eulerAngles = origPos;
        mRotating = true;
        mRotationX = mOrigRot.x;
        mRotationY = mOrigRot.y;
    }
}
