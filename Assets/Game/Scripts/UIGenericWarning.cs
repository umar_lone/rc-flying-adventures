﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class UIGenericWarning : MonoBehaviour {
    public GameObject missionFailedUI;
    private string mMissionText;
    private Text mTextMessage;
    private bool mWarningCondition=true;
    private GameObject mCanvas;
    private float mOldElapsedSeconds;
    private string mWarningMessage;
    private int mWarningTime;
    private string mFailureMessage;
    // Use this for initialization
    void Start() {
        mWarningCondition = true;
        mCanvas = transform.FindChild("Canvas").gameObject;
        mTextMessage = transform.FindChild("Canvas/Content/PanelChild/Text-Content").gameObject.GetComponent<Text>();
        StartCoroutine(Countdown(mOldElapsedSeconds));
    }
    public void SetWarningMessage(string message) {
        mWarningMessage = message;
    }
    public void SetWarningTime(int time) {
        mWarningTime = time;
        mOldElapsedSeconds = mWarningTime;
    }
    public void SetFailureMessage(string message) {
        mFailureMessage = message;
    }
    public void StopWarning() {
        mWarningCondition = false;
    }
    public void PauseCountdown() {
        gameObject.SetActive(false);
    }
    public void ResumeCountdown() {
        gameObject.SetActive(true);
        StartCoroutine(Countdown(mOldElapsedSeconds));
    }
    private IEnumerator Countdown(float warningTime) {
        float mSecondsElapsed =warningTime;

        while (mSecondsElapsed > -1f) {
            mTextMessage.text = mWarningMessage + "\n" + mSecondsElapsed;
            yield return CoroutineUtilities.WaitForRealTime(1);
            mOldElapsedSeconds = --mSecondsElapsed;
            if(mWarningCondition == false) {
                gameObject.SetActive(false);
                GameObject.Destroy(gameObject);
                yield return null;
            }
        }
        //Turn engine power off and wait for 5 seconds and then show the restart menu
        GameManager.Instance.GetCurrentVehicle().GetComponent<HelicopterController>().Crash();
        mCanvas.SetActive(false);
        StartCoroutine(CustomSleep(5));
        yield return null;
    }

    private IEnumerator CustomSleep(int v) {
        yield return CoroutineUtilities.WaitForRealTime(v);
        GameObject go = Instantiate(missionFailedUI);
        go.GetComponent<PauseController>().SetFailureMessage(mFailureMessage);
        GameObject.Destroy(gameObject);
        yield return null;
    }
}
