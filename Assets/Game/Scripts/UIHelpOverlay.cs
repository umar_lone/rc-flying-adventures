﻿using UnityEngine;
using System.Collections;

public class UIHelpOverlay : MonoBehaviour {

    public GameObject optionsUI;
    public GameObject dialog;
    private float mOldTimeScale;
    private GameObject mWarningUI;
    void Start() {
        GameManager.Instance.PauseAudioSources();

        /*
        When the pause menu comes up, the mission info dialog
        should be destroyed immediately to prevent it from
        overlapping the pause menu.
        */
        GameObject missionInfo = GameObject.FindGameObjectWithTag("UI_MISSIONINFO");
        if (missionInfo != null) {
            GameObject.Destroy(missionInfo);
        }

        /*
        Same thing for warning ui, but we only disable it and re-enable when
        user resumes.
        */
        mWarningUI = GameObject.FindGameObjectWithTag("UI_WARNING");
        if (mWarningUI != null) {
            mWarningUI.GetComponent<UIWarning>().PauseCountdown();
        }
        mOldTimeScale = GameManager.Instance.GetTimeScale();
        GameManager.Instance.SetTimeScale(0f);
        MakeTransparent(gameObject);
        StartCoroutine(FadeIn(gameObject));
    }

    public void OnResume() {
        StartCoroutine(FadeOut(gameObject));
        GameManager.Instance.SetTimeScale(mOldTimeScale);
        GameObject.Destroy(gameObject, 0.5f);
        /*
        Maybe user paused while the warning was being displayed,
        so set that warning active
        */
        if (mWarningUI != null)
            mWarningUI.GetComponent<UIWarning>().ResumeCountdown();
        GameManager.Instance.ResumeAudioSources();
    }
    private void MakeTransparent(GameObject go) {
        go.transform.FindChild("Canvas").gameObject.GetComponent<CanvasGroup>().alpha = 0f;
    }

    private IEnumerator FadeIn(GameObject incoming) {
        incoming.SetActive(true);
        CanvasGroup fadein = incoming.transform.FindChild("Canvas").GetComponent<CanvasGroup>();
        while (fadein.alpha < 1) {
            fadein.alpha += 0.1f;
            yield return CoroutineUtilities.WaitForRealTime(0.001f);
        }
        fadein.interactable = true;
        yield return null;
    }
    private IEnumerator FadeOut(GameObject outgoing) {
        CanvasGroup fadeout = outgoing.transform.FindChild("Canvas").GetComponent<CanvasGroup>();
        fadeout.interactable = false;

        while (fadeout.alpha > 0) {
            fadeout.alpha -= 0.1f;
            yield return CoroutineUtilities.WaitForRealTime(0.001f);

        }
        outgoing.SetActive(false);
        yield return null;
    }
}
