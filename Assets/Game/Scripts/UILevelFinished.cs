﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System;

public class UILevelFinished : MonoBehaviour {
    public GameObject simpleOkDialog;
    public GameObject optionsUI;
    public GameObject dialog;
    private Text mTextMessage;
    private string mMessage;
	// Use this for initialization
	void Start () {
        GameManager.Instance.PauseAudioSources();
        GameManager.Instance.SetCrossHairVisibility(false);
        mTextMessage = transform.FindChild("Canvas/Panel-Main/Text-Message").GetComponent<Text>();

        int currentLevel = GameManager.Instance.GetCurrentLevelIndex();
        int currentScene = GameManager.Instance.GetCurrentSceneIndex();


        /*
        Max levels are 10 for scene0. currentLevel will always reach 9 only as it starts from 0, so 
        add 1 before comparing with GetMaxLevelsInScene
        */
        if ((currentLevel+1) >= GameManager.Instance.GetMaxLevelsInScene(currentScene)) {
            transform.FindChild("Canvas/Panel-Main/ButtonNext").gameObject.SetActive(false);
            GameObject go = Instantiate(simpleOkDialog);
            UISimpleOkDialog missionInfo = go.GetComponent<UISimpleOkDialog>();
            missionInfo.SetTitle("Info");
            missionInfo.SetMessage("Congrats. You've finished this episode. Watch out for more episodes and levels in future updates!");
            //mTextMessage.text = "You've finished all the levels in this episode. Watch out for more updates";
            //Disable next mission button
        }
        else {
            GameManager.Instance.UnlockNextLevelForScene(currentScene);
        }
        StartCoroutine(ShowMessage());
        GameManager.Instance.SetTimeScale(0f);
	}

    private IEnumerator ShowMessage() {
        mTextMessage.text = mMessage;
        yield return null;
    }

    public void SetMessage(string msg) {
        mMessage = msg;
    }
    public void OnNext() {
        GameManager.Instance.LoadNextScene();
    }
    public void OnOptions() {
        Instantiate(optionsUI);
    }
    public void OnRestart() {
        GameObject messageBox = Instantiate(dialog);
        UIMessageDialog dlg = messageBox.GetComponent<UIMessageDialog>();
        dlg.SetMessage("Are you sure you want to restart?");
        dlg.onNoClicked += () => { };
        dlg.onYesClicked += () => { GameManager.Instance.RestartScene(); };
    }
    public void OnMenu() {
        GameObject messageBox = Instantiate(dialog);
        UIMessageDialog dlg = messageBox.GetComponent<UIMessageDialog>();
        dlg.SetMessage("Go to main menu?");
        dlg.onNoClicked += () => { };
        dlg.onYesClicked += () => { GameManager.Instance.ShowMainMenu(); };
    }
}
