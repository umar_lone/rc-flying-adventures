﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class UIMissionInfo : MonoBehaviour {
    private bool isDimissing = false ;
    private string mMessage;
    private string mTitle;
    private static UIMissionInfo mInstance = null;
    private int mAutoDestructTime = 4;
	// Use this for initialization
	void Start () {
        /*
        Only one instance of this class should exist at a time.
        If mInstance is not null, it means the dialog is being displayed, so
        don't allow another dialog to be instantiated
        */
        if (mInstance != null) {
            GameObject.DestroyImmediate(gameObject);
            return;
        } else {
            mInstance = this;
            MakeTransparent(gameObject);
            StartCoroutine(FadeIn(gameObject));
            StartCoroutine(AutoDismiss(mAutoDestructTime));
        }
    }
    public void SetAutDestructTime(int time) {
        mAutoDestructTime = time;
    }
    public void SetMissionText(string text) {
        mMessage = text;
    }
    public void SetTitle(string text) {
        mTitle = text;
    }
    private IEnumerator AutoDismiss(int v) {
        yield return CoroutineUtilities.WaitForRealTime(v);
        if (isDimissing) //It has already started dismissing, so don't do anything
            yield return null;
        //Else start dismissing and also indicate the status through isDismissing flag
        isDimissing = true;
        StartCoroutine(FadeOut(gameObject));
        GameObject.Destroy(gameObject, 0.5f);
        mInstance = null;
    }

    private void MakeTransparent(GameObject go) {
        go.transform.FindChild("Canvas").gameObject.GetComponent<CanvasGroup>().alpha = 0f;
    }
    private IEnumerator FadeIn(GameObject incoming) {
        incoming.SetActive(true);
        Text textTitle = transform.FindChild("Canvas/Content/PanelChild/Text-Title").gameObject.GetComponent<Text>();
        textTitle.text = mTitle;
        Text textMessage = transform.FindChild("Canvas/Content/PanelChild/Text-Content").gameObject.GetComponent<Text>();
        textMessage.text = mMessage;
        CanvasGroup fadein = incoming.transform.FindChild("Canvas").GetComponent<CanvasGroup>();
        while (fadein.alpha < 1) {
            fadein.alpha += 0.1f;
            yield return CoroutineUtilities.WaitForRealTime(0.01f);
        }
        fadein.interactable = true;
        yield return null;
    }
    private IEnumerator FadeOut(GameObject outgoing) {
        CanvasGroup fadeout = outgoing.transform.FindChild("Canvas").GetComponent<CanvasGroup>();
        fadeout.interactable = false;

        while (fadeout.alpha > 0) {
            fadeout.alpha -= 0.1f;
            yield return CoroutineUtilities.WaitForRealTime(0.01f);

        }
        outgoing.SetActive(false);
        yield return null;
    }
    public void OnClickDismiss() {
        if (isDimissing) //It has already started dismissing, so don't do anything
            return;
        //Else start dismissing and also indicate the status through isDismissing flag
        isDimissing = true;
        //Dismiss the mission info
        StartCoroutine(FadeOut(gameObject));
        GameObject.Destroy(gameObject, 0.5f);
    }
}
