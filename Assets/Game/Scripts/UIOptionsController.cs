﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIOptionsController : MonoBehaviour {
    private Slider qualitySlider;
    private Toggle toggleMusic;
    private Toggle toggleSound;
    private Button mConfigAccelButton;
    

    //Video
    private Slider mSliderShadowRez;
    private Slider mSliderAntiAliasing;
    private Slider mSliderTextureRez;
    private Slider mSliderDrawDistance;
    private Slider mSliderPixelLights;
    private Toggle mToggleSoftVeg;

    //Audio
    private Slider mSliderMusic;
    private Slider mSliderSound;

    //Input
    private Toggle mToggleTouch;
    private Toggle mToggleAccel;

    private GameObject mAccelConfig;

    //Time
    private Toggle mToggleSystemTime;
    private Toggle mToggleFastTime;
    private Toggle mToggleDayTime;
    private Toggle mToggleNightTime;

    //Camera
    private Toggle mToggleCamNear;
    private Toggle mToggleCamFar;
    private Toggle mToggleCamVeryFar;


    private GameObject mMain;
    private GameObject mAudio;
    private GameObject mGraphics;
    private GameObject mInput;
    private GameObject mTime;

    private bool mAntiAliasingInitialized;
    // Use this for initialization
    void Start () {
        InitializeUI();
        UpdateUIValues();
    }

    private void InitializeUI() {
        mAntiAliasingInitialized = false;
        mMain = transform.FindChild("_Main").gameObject;
        MakeTransparent(mMain);
        StartCoroutine(FadeIn(mMain));

        mAudio = transform.FindChild("Audio").gameObject;
        MakeTransparent(mAudio);
        mAudio.SetActive(false);

        mGraphics = transform.FindChild("Graphics").gameObject;
        MakeTransparent(mGraphics);
        mGraphics.SetActive(false);

        mInput = transform.FindChild("Input").gameObject;
        MakeTransparent(mInput);
        mInput.SetActive(false);

        mAccelConfig = mInput.transform.FindChild("AccelConfig").gameObject;
        MakeTransparent(mAccelConfig);
        mAccelConfig.SetActive(false);

        mTime = transform.FindChild("Time").gameObject;
        MakeTransparent(mTime);
        mTime.SetActive(false);

        mToggleSystemTime = transform.FindChild("Time/Canvas/Panel-Main/Toggle-System").gameObject.GetComponent<Toggle>();
        mToggleFastTime = transform.FindChild("Time/Canvas/Panel-Main/Toggle-Fast").gameObject.GetComponent<Toggle>();
        mToggleDayTime = transform.FindChild("Time/Canvas/Panel-Main/Toggle-Day").gameObject.GetComponent<Toggle>();
        mToggleNightTime = transform.FindChild("Time/Canvas/Panel-Main/Toggle-Night").gameObject.GetComponent<Toggle>();

        mToggleCamNear = transform.FindChild("Time/Canvas/Panel-Main/Toggle-CameraNear").gameObject.GetComponent<Toggle>();
        mToggleCamFar = transform.FindChild("Time/Canvas/Panel-Main/Toggle-CameraFar").gameObject.GetComponent<Toggle>();
        mToggleCamVeryFar = transform.FindChild("Time/Canvas/Panel-Main/Toggle-CameraVeryFar").gameObject.GetComponent<Toggle>();

        mSliderShadowRez = transform.FindChild("Graphics/Canvas/Panel-Main/ScrollView/Content/Slider-ShadowRez").gameObject.GetComponent<Slider>();
        mSliderAntiAliasing = transform.FindChild("Graphics/Canvas/Panel-Main/ScrollView/Content/Slider-Antialias").gameObject.GetComponent<Slider>();
        mSliderTextureRez = transform.FindChild("Graphics/Canvas/Panel-Main/ScrollView/Content/Slider-TextureRez").gameObject.GetComponent<Slider>();
        mSliderDrawDistance = transform.FindChild("Graphics/Canvas/Panel-Main/ScrollView/Content/Slider-DrawDistance").gameObject.GetComponent<Slider>();
        mSliderPixelLights = transform.FindChild("Graphics/Canvas/Panel-Main/ScrollView/Content/Slider-PixelLights").gameObject.GetComponent<Slider>();
        mToggleSoftVeg = transform.FindChild("Graphics/Canvas/Panel-Main/ScrollView/Content/ToggleCheck-SoftVeg").gameObject.GetComponent<Toggle>();

        mSliderMusic = transform.FindChild("Audio/Canvas/Panel-Main/Slider-Music").gameObject.GetComponent<Slider>();
        mSliderSound = transform.FindChild("Audio/Canvas/Panel-Main/Slider-Sound").gameObject.GetComponent<Slider>();


        mToggleTouch = transform.FindChild("Input/Canvas/Panel-Main/Toggle-Touch").gameObject.GetComponent<Toggle>();
        mToggleAccel = transform.FindChild("Input/Canvas/Panel-Main/Toggle-Accel").gameObject.GetComponent<Toggle>();
        mConfigAccelButton = transform.FindChild("Input/Canvas/Panel-Main/ButtonConfigAccel").gameObject.GetComponent<Button>();

    }

    private void UpdateUIValues() {
        //Set the values
        mSliderDrawDistance.value = GameManager.Instance.GetDrawDistance();
        mSliderPixelLights.value = GameManager.Instance.GetPixelLightCount();
        mSliderShadowRez.value = GameManager.Instance.GetShadowLevelSetting();
        mSliderAntiAliasing.value = GameManager.Instance.GetAntiAliasingSetting();
        mAntiAliasingInitialized = true;

        /*
        Texture resolution is 0 for full, 1 for half and 2 for quarter, so
        subtract 2 the correct level
        */
        mSliderTextureRez.value = 2 - GameManager.Instance.GetTextureRezSetting();
        mToggleSoftVeg.isOn = GameManager.Instance.GetSoftVegSetting();

        mSliderMusic.value = GameManager.Instance.GetMusicSetting();
        mSliderSound.value = GameManager.Instance.GetSoundSetting();


        mAccelConfig.SetActive(false);
        int inputType = GameManager.Instance.GetInputType();
        switch (inputType) {
            case 0:
                mToggleTouch.isOn = true;
                mToggleAccel.isOn = false;
                mConfigAccelButton.GetComponent<Button>().interactable = false;
                break;
            case 1:
                mToggleTouch.isOn = false;
                mToggleAccel.isOn = true;
                mConfigAccelButton.GetComponent<Button>().interactable = true;
                break;
        }

        int time = GameManager.Instance.GetTimeOfDaySetting();
        switch (time) {
            case 0:     //System
                mToggleSystemTime.isOn = true;
                mToggleFastTime.isOn = false;
                mToggleDayTime.isOn = false;
                mToggleNightTime.isOn = false;
                break;
            case 1:     //Fast
                mToggleSystemTime.isOn = false;
                mToggleFastTime.isOn = true;
                mToggleDayTime.isOn = false;
                mToggleNightTime.isOn = false;
                break;
            case 2:     //Day
                mToggleSystemTime.isOn = false;
                mToggleFastTime.isOn = false;
                mToggleDayTime.isOn = true;
                mToggleNightTime.isOn = false;
                break;
            case 3:     //Night
                mToggleSystemTime.isOn = false;
                mToggleFastTime.isOn = false;
                mToggleDayTime.isOn = false;
                mToggleNightTime.isOn = true;
                break;
        }
        int cameraPosition = GameManager.Instance.GetCameraPosition();
        switch (cameraPosition) {
            case 0:
                mToggleCamNear.isOn = true;
                break;
            case 1:
                mToggleCamFar.isOn = true;
                break;
            case 2:
                mToggleCamVeryFar.isOn = true;
                break;
        }
    }

    private void MakeTransparent(GameObject go) {
        go.transform.FindChild("Canvas").gameObject.GetComponent<CanvasGroup>().alpha = 0f ;
    }

    private IEnumerator FadeIn(GameObject incoming) {
        incoming.SetActive(true);
        CanvasGroup fadein = incoming.transform.FindChild("Canvas").GetComponent<CanvasGroup>();
        while (fadein.alpha < 1) {
            fadein.alpha += 0.1f;
            yield return CoroutineUtilities.WaitForRealTime(0.001f);
        }
        fadein.interactable = true;
        yield return null;
    }
    private IEnumerator FadeOut(GameObject outgoing) {
        CanvasGroup fadeout = outgoing.transform.FindChild("Canvas").GetComponent<CanvasGroup>();
        fadeout.interactable = false;

        while (fadeout.alpha > 0) {
            fadeout.alpha -= 0.1f;
            yield return CoroutineUtilities.WaitForRealTime(0.001f);

        }
        outgoing.SetActive(false);
        yield return null;
    }
    public void OnClickInput() {
        StartCoroutine(FadeIn(mInput));
    }
    public void OnClickTime() {
        StartCoroutine(FadeIn(mTime));
    }
    public void OnClickAudio() {
        StartCoroutine(FadeIn(mAudio));
    }
    public void OnClickGraphics() {
        StartCoroutine(FadeIn(mGraphics));
    }

    #region SETTINGS BACK HANDLERS
    public void OnClickInputBack() {
        StartCoroutine(FadeOut(mInput));
    }
    public void OnClickTimeBack() {
        StartCoroutine(FadeOut(mTime));
    }
    public void OnClickAudioBack() {
        StartCoroutine(FadeOut(mAudio));
    }
    public void OnClickGraphicsBack() {
        StartCoroutine(FadeOut(mGraphics));
    }
    public void OnClickBack() {
        StartCoroutine(FadeOut(mMain));
        GameObject.Destroy(gameObject, .5f);
    }
    #endregion

    #region AUDIO/VIDEO HANDLERS

    public void OnToggleSoftVegetation() {
        GameManager.Instance.SetSoftVegSetting(mToggleSoftVeg.isOn);
    }
    public void OnShadowRezChanged() {

        GameManager.Instance.SetShadowLevelSetting((int)mSliderShadowRez.value);
    }
    public void OnTextureRezChanged() {
        /*
        Texture resolution is 0 for full, 1 for half and 2 for quarter, so
        subtract 2 the correct level
        */
        GameManager.Instance.SetTextureRezSetting(2-(int)mSliderTextureRez.value);
    }
    public void OnAntiAliasingChanged() {
        if (!mAntiAliasingInitialized)
            return;
        //Get exponential value as 0, 2x, 4x, 8x
        int value = (int)Mathf.Pow(2, mSliderAntiAliasing.value);
        //2^0 will be 1, so use 0 manually
        GameManager.Instance.SetAntiAliasingSetting((value == 1 ? 0 : value));
    }
    public void OnDrawDistanceChanged() {
        GameManager.Instance.SetDrawDistance((int)mSliderDrawDistance.value);
    }
    public void OnPixelLightsChanged() {
        GameManager.Instance.SetPixelLightCount((int)mSliderPixelLights.value);
    }
    public void OnMusicVolumeChanged() {
        GameManager.Instance.SetMusicSetting(mSliderMusic.value);
    }
    public void OnSoundVolumeChanged() {
        GameManager.Instance.SetSoundSetting(mSliderSound.value);
    }

    #endregion
    #region INPUT HANDLERS
    public void OnToggleInputOptions() {
        GameObject movementControls = GameObject.Find("ETCOnScreenControls/SimpleControls/MovementControls");
        if (mToggleTouch.isOn) {
            if (movementControls != null)
                movementControls.SetActive(true);
            GameManager.Instance.SetInputType(0);
            mConfigAccelButton.GetComponent<Button>().interactable = false;
        }
        else if (mToggleAccel.isOn) {
            if (movementControls != null)
                movementControls.SetActive(false);
            GameManager.Instance.SetInputType(1);
            mConfigAccelButton.GetComponent<Button>().interactable = true;
        }
    }
    public void OnConfigureAccelInput() {
        StartCoroutine(FadeIn(mAccelConfig));
    }
  
    public void OnApplyAccelChanges() {
        GameManager.Instance.SetAccelOffset(Input.acceleration);
        StartCoroutine(FadeOut(mAccelConfig));
    }
    public void OnOk() {
        StartCoroutine(FadeOut(mMain));
        GameObject.Destroy(gameObject, 5);
    }
    #endregion
    #region GAME
    public void OnTimeOfDayChanged() {
        int index = 0;
        if (mToggleSystemTime.isOn) {
            index = 0;
        }else if (mToggleFastTime.isOn) {
            index = 1;
        }else if (mToggleDayTime.isOn) {
            index = 2;
        }else if (mToggleNightTime.isOn) {
            index = 3;
        } else {
            //TODO Maybe remove this later???
            throw new System.Exception("None of the radio buttons are checked");
        }
        GameManager.Instance.SetTimeOfDaySetting(index);
    }
    public void OnCameraPositionChanged() {
        int index = 0;
        if (mToggleCamNear.isOn) {
            index = 0;
        } else if (mToggleCamFar.isOn) {
            index = 1;
        } else if (mToggleCamVeryFar.isOn) {
            index = 2;
        }
        GameManager.Instance.SetCameraPosition(index);
    }
    #endregion
}
