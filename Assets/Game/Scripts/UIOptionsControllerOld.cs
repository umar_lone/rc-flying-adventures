﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIOptionsControllerOld : MonoBehaviour {
    private Slider qualitySlider;
    private Slider timeSlider;
    private Toggle toggleSoftVeg;
    private Toggle toggleMusic;
    private Toggle toggleSound;
    private Button configButton;
    private Toggle toggleTouch;
    private Toggle toggleAccel;
    private GameObject configPanel;

    // Use this for initialization
    void Start () {
        qualitySlider = transform.FindChild("Canvas/Panel/PanelAV/Slider").gameObject.GetComponent<Slider>();
        timeSlider = transform.FindChild("Canvas/Panel/PanelDayCycle/Slider").gameObject.GetComponent<Slider>();
        toggleSoftVeg = transform.FindChild("Canvas/Panel/PanelAV/ToggleCheck-SoftVeg").gameObject.GetComponent<Toggle>();
        toggleMusic = transform.FindChild("Canvas/Panel/PanelAV/ToggleCheck-Music").gameObject.GetComponent<Toggle>();
        toggleSound = transform.FindChild("Canvas/Panel/PanelAV/ToggleCheck-Sounds").gameObject.GetComponent<Toggle>();
        toggleTouch = transform.FindChild("Canvas/Panel/PanelInput/Toggle-Touch").gameObject.GetComponent<Toggle>();
        toggleAccel = transform.FindChild("Canvas/Panel/PanelInput/Toggle-Accel").gameObject.GetComponent<Toggle>();

        configPanel = transform.FindChild("Canvas/Panel/PanelConfigAccel").gameObject;
        configButton = transform.FindChild("Canvas/Panel/PanelInput/ButtonConfigureInput").gameObject.GetComponent<Button>();

        //qualitySlider.value = GameManager.Instance.GetQualityLevelSetting();
        //timeSlider.value = GameManager.Instance.GetTimeOfDaySetting();
        //toggleMusic.isOn = GameManager.Instance.GetMusicSetting();
        //toggleSound.isOn = GameManager.Instance.GetSoundSetting();
        toggleSoftVeg.isOn = GameManager.Instance.GetSoftVegSetting();
        configPanel.SetActive(false);
        int inputType = GameManager.Instance.GetInputType();
        switch (inputType) {
            case 0:
                toggleTouch.isOn = true;
                toggleAccel.isOn = false;
                configButton.GetComponent<Button>().interactable = false;
                break;
            case 1:
                toggleTouch.isOn = false;
                toggleAccel.isOn = true;
                configButton.GetComponent<Button>().interactable = true;
                break;
        }
    }
	
    #region AUDIO/VIDEO HANDLERS
    public void OnToggleMusic() {
        //GameManager.Instance.SetMusicSetting(toggleMusic.isOn);
    }
    public void OnToggleSounds() {
        //GameManager.Instance.SetSoundSetting(toggleSound.isOn);
    }
    public void OnToggleSoftVegetation() {
        GameManager.Instance.SetSoftVegSetting(toggleSoftVeg.isOn);
    }
    public void OnQualityChanged() {
        //GameManager.Instance.SetQualityLevelSetting((int)qualitySlider.value);
    }
    #endregion
    #region INPUT HANDLERS
    public void OnToggleInputOptions() {
        GameObject movementControls = GameObject.Find("ETCOnScreenControls/SimpleControls/MovementControls");
        if (toggleTouch.isOn) {
            if (movementControls != null)
                movementControls.SetActive(true);
            GameManager.Instance.SetInputType(0);
            configButton.GetComponent<Button>().interactable = false;
        }
        else if (toggleAccel.isOn) {
            if (movementControls != null)
                movementControls.SetActive(false);
            GameManager.Instance.SetInputType(1);
            configButton.GetComponent<Button>().interactable = true;
        }
    }
    public void OnConfigureInput() {
        configPanel.SetActive(true);
    }
  
    public void OnApplyAccelChanges() {
        configPanel.SetActive(false);
        GameManager.Instance.SetAccelOffset(Input.acceleration);

    }
    public void OnOk() {
        GameObject.Destroy(gameObject);
    }
    #endregion
    #region GAME
    public void OnTimeOfDayChanged() {
        GameManager.Instance.SetTimeOfDaySetting((int)timeSlider.value);
        GameObject go = GameObject.Find("DayNightController");
        if (go != null) {
            DayNightController dnc = go.GetComponent<DayNightController>();
            dnc.SetTimeOfDayOption();
        }//else the user has opened the menu from the Main Menu, so don't do anything
    }
    #endregion
}
