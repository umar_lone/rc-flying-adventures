﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIMessageDialog : MonoBehaviour {
    public delegate void YesClick();
    public delegate void NoClick();
    public event YesClick onYesClicked;
    public event NoClick onNoClicked;
    private string mMessage;
    private Text mTextMessage;
    void Start() {
        MakeTransparent(gameObject);
        StartCoroutine(FadeIn(gameObject));
    }
    public void SetMessage(string message) {
        mMessage = message;
    }
    public void OnClickNo() {
        if(onNoClicked != null) {
            onNoClicked();
        }
        Close();
    }
    public void OnClickYes() {
        if(onYesClicked != null) {
            onYesClicked();
        }
        Close();
    }
    private void Close() {
        StartCoroutine(FadeOut(gameObject));
        GameObject.Destroy(gameObject, 0.5f);
    }
    private void MakeTransparent(GameObject go) {
        go.transform.FindChild("Canvas").gameObject.GetComponent<CanvasGroup>().alpha = 0f;
    }

    private IEnumerator FadeIn(GameObject incoming) {
        incoming.SetActive(true);
        mTextMessage = transform.FindChild("Canvas/Content/PanelChild/Text-Content").gameObject.GetComponent<Text>();
        mTextMessage.text = mMessage;
        CanvasGroup fadein = incoming.transform.FindChild("Canvas").GetComponent<CanvasGroup>();
        while (fadein.alpha < 1) {
            fadein.alpha += 0.1f;
            yield return CoroutineUtilities.WaitForRealTime(0.001f);
        }
        fadein.interactable = true;
        yield return null;
    }
    private IEnumerator FadeOut(GameObject outgoing) {
        CanvasGroup fadeout = outgoing.transform.FindChild("Canvas").GetComponent<CanvasGroup>();
        fadeout.interactable = false;

        while (fadeout.alpha > 0) {
            fadeout.alpha -= 0.1f;
            yield return CoroutineUtilities.WaitForRealTime(0.001f);

        }
        outgoing.SetActive(false);
        yield return null;
    }
}
