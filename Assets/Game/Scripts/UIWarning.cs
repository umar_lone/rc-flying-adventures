﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class UIWarning : MonoBehaviour {
    public GameObject missionFailedUI;
    public const int WARNING_TIME = 8;
    private string mMissionText;
    private Text mTextMessage;
    private bool mLowSignal=true;
    private GameObject mCanvas;
    private float mOldElapsedSeconds;
    // Use this for initialization
    void Start() {
        mOldElapsedSeconds = WARNING_TIME;
        mLowSignal = true;
        mCanvas = transform.FindChild("Canvas").gameObject;
        mTextMessage = transform.FindChild("Canvas/Content/PanelChild/Text-Content").gameObject.GetComponent<Text>();
        StartCoroutine(Countdown(mOldElapsedSeconds));
    }
    public void StopWarning() {
        mLowSignal = false;
    }
    public void PauseCountdown() {
        gameObject.SetActive(false);
    }
    public void ResumeCountdown() {
        gameObject.SetActive(true);
        StartCoroutine(Countdown(mOldElapsedSeconds));
    }
    private IEnumerator Countdown(float warningTime) {
        float mSecondsElapsed =warningTime;

        while (mSecondsElapsed > -1f) {
            mTextMessage.text = string.Format("Low signal! Turn back immediately\n{0}", mSecondsElapsed);
            yield return CoroutineUtilities.WaitForRealTime(1);
            mOldElapsedSeconds = --mSecondsElapsed;
            if(mLowSignal == false) {
                gameObject.SetActive(false);
                GameObject.Destroy(gameObject);
                yield return null;
            }
        }
        //Turn engine power off and wait for 3 seconds and then show the restart menu
        GameManager.Instance.GetCurrentVehicle().GetComponent<HelicopterController>().Crash();
        mCanvas.SetActive(false);
        StartCoroutine(CustomSleep(5));
        yield return null;
    }

    private IEnumerator CustomSleep(int v) {
        yield return CoroutineUtilities.WaitForRealTime(v);
        GameObject go = Instantiate(missionFailedUI);
        go.GetComponent<PauseController>().SetFailureMessage("The helicopter crashed");
        GameObject.Destroy(gameObject);
        yield return null;
    }
}
