﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class VehicleExtraControls : MonoBehaviour {
    private Image mToggleBackgroundImage;
    private Swiper mSwiper;
    private Toggle mLightToggle;
    public Text vehiclePanel;
    public LayerMask terrainMask;

    private Vector3 mPreviousPosition;
    private Vector3 mCurrentPosition;
    // Use this for initialization
    void Start() {
        GameObject dayController = GameObject.Find("DayNightController");
        DayNightController controller = dayController.GetComponent<DayNightController>();
        controller.dayTimeChanged += OnLightChanged;

        GameObject currentVehicle = GameManager.Instance.GetCurrentVehicle().transform.parent.gameObject;
        mSwiper = currentVehicle.transform.FindChild("FollowCamera/Main Camera").GetComponent<Swiper>();

        GameObject go = GameObject.Find("ETCOnScreenControls");
        GameObject controls = go.transform.FindChild("AttackHeliControls").gameObject;
        mToggleBackgroundImage = go.transform.FindChild("OnScreenUI/Toggle-2x/Background").GetComponent<Image>();
        switch (GameManager.Instance.GetVehicleType()) {
            case VehicleType.SIMPLEHELICOPTER:
                controls.SetActive(false);
                break;
            case VehicleType.ATTACKHELICOPTER:
                controls.SetActive(true);
                break;
        }
        mLightToggle = go.transform.FindChild("OnScreenUI/Toggle-Flashlight").GetComponent<Toggle>();
        controller.NotifyTimeListeners();
        StartCoroutine(VehicleInfo());
    }

    private IEnumerator VehicleInfo() {
        Vector3 velocity;
        Vector3 previousPosition = GameManager.Instance.GetCurrentVehicle().transform.position;
        while (true) {
            velocity = GameManager.Instance.GetCurrentVehicle().transform.position - previousPosition;
            previousPosition = GameManager.Instance.GetCurrentVehicle().transform.position;

            RaycastHit hit;
            float heightAboveGround = 0;

            if (Physics.Raycast(GameManager.Instance.GetCurrentVehicle().transform.position,
                GameManager.Instance.GetCurrentVehicle().transform.TransformDirection(Vector3.down),
                out hit,
                Mathf.Infinity,
                terrainMask)) {
                heightAboveGround = hit.distance;
            }
            Vector2 vel = new Vector2(velocity.x, velocity.z);
            string vehicleParams = string.Format("{0:0.00} m\n{1:0.00} m/s", heightAboveGround, vel.magnitude);
            vehiclePanel.text = vehicleParams;
            yield return new WaitForSeconds(.2f);
        }
    }


    private void SetVehicleLight(bool state) {
        GameObject vehicle = GameManager.Instance.GetCurrentVehicle();
        if (vehicle == null) {
            return;
        }

        LightSource light = vehicle.transform.FindChild("Headlight").GetComponent<LightSource>();
        light.SetLightState(state);
        mLightToggle.isOn = state;
    }

    public void OnToggleFlashLight(bool state) {
        /*
        This is required only when we're running the scene directly.
        In most cases, our GameManager may be instantiated AFTER the
        current game object. This won't be a problem while running the 
        game, as the GameManager instance will already have been created in
        the main menu
        */
        if (GameManager.Instance == null)
            return;

        /*
        The UI toggle gets called on scene load ,before the 
        CurrentLevelManager where the current vehicle is set 
        inside the GameManager. So, ignore for the first time.
        */
        GameObject go = GameManager.Instance.GetCurrentVehicle();
        if (go == null) {
            return;
        }

        LightSource light = go.transform.FindChild("Headlight").GetComponent<LightSource>();
        light.SetLightState(state);

    }
    public void OnToggleFastForward(bool state) {
        if (state) {
            mToggleBackgroundImage.color = new Color(0, 0, 0, 0);
            GameManager.Instance.SetTimeScale(2f);
        }
        else {
            mToggleBackgroundImage.color = Color.white;
            //TODO Required only when the Farm scene is run directly
            if (GameManager.Instance != null)
                GameManager.Instance.SetTimeScale(1f);
        }
    }
    public void OnFire() {
        GameObject go = GameManager.Instance.GetCurrentVehicle();
        WeaponSystem ws = go.GetComponent<WeaponSystem>();
        ws.Fire();

    }
    public void OnResetCamera() {
        mSwiper.ResetCamera();
    }
    private void OnLightChanged(DayNightController.TimeOfDay time) {
        switch (time) {
            case DayNightController.TimeOfDay.DAY:
                SetVehicleLight(false);
                break;
            case DayNightController.TimeOfDay.NIGHT:
            case DayNightController.TimeOfDay.DUSK:
            case DayNightController.TimeOfDay.DAWN:
                SetVehicleLight(true);
                break;
            default:
                break;
        }
    }
}
