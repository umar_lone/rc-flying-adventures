﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class VehicleLight : MonoBehaviour {
    private Toggle mToggleFlashlight;
    private Light mLight;
    private bool mLightToggled = false;
    // Use this for initialization
    void Start() {
        GameObject go = GameObject.Find("ETCOnScreenControls");
        mToggleFlashlight = go.transform.FindChild("OnScreenUI/Toggle-Flashlight").GetComponent<Toggle>();

        GameObject dayController = GameObject.Find("DayNightController");
        DayNightController controller = dayController.GetComponent<DayNightController>();
        controller.dayTimeChanged += OnLightChanged;
        mLight = transform.GetComponentInChildren<Light>();
        
        if (gameObject.name == "Headlight" || gameObject.name == "AmbientLight") {
            controller.NotifyTimeListeners();
        }


    }
    public void SetLightState(bool state) {
        mLightToggled = true;
        mLight.enabled = state;
    }
    private void OnLightChanged(DayNightController.TimeOfDay time) {
        if (mLightToggled)
            return ;
        switch (time) {
            case DayNightController.TimeOfDay.DAY:
                mLight.enabled = false;
                mToggleFlashlight.isOn = false;
                break;
            case DayNightController.TimeOfDay.NIGHT:
            case DayNightController.TimeOfDay.DUSK:
            case DayNightController.TimeOfDay.DAWN:
                mLight.enabled = true;
                mToggleFlashlight.isOn = true;
                break;
            default:
                break;
        }
    }


}
