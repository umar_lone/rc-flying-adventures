﻿using UnityEngine;
using System.Collections;

public class WeaponSystem : MonoBehaviour {
    private GameObject crosshairProxy;
    public float crosshairDistance = 8;
    public Texture2D crosshairTexture;
    public Camera helicopterCamera;
    public GameObject missilePrefab;
    private Transform tRight;
    private Transform tLeft;
    [Range(.1f, 1f)]
    public float timescale = 1.0f;
    private int mBarrel = 0; //0 is left & 1 is right
    private bool mCrossHairVisible = true;

    public int mAvailableDarts = 10;
    // Use this for initialization
    void Start() {
        crosshairProxy = transform.FindChild("CrossHairPlaceholder").gameObject;

        crosshairProxy.transform.Translate(transform.forward * crosshairDistance);
        Vector3 pos = crosshairProxy.transform.position;
        pos.y += 5;
        //crosshairProxy.transform.position = pos;
        tRight = transform.FindChild("TargettingSystemRight");
        tLeft = transform.FindChild("TargettingSystemLeft");
    }

    // Update is called once per frame
    void Update() {
        //Used only for testing projectiles
        //Time.timeScale = timescale;
       

        if (Input.GetKeyDown(KeyCode.K)) {
            Fire();
            //tLeft.transform.LookAt(crosshairProxy.transform);
            //Instantiate(missilePrefab, tLeft.position, tLeft.rotation);
        }
        if (Input.GetKeyDown(KeyCode.L)) {
            //tRight.transform.LookAt(crosshairProxy.transform);
            //Instantiate(missilePrefab, tRight.position, tRight.rotation);
            Fire();
        }

        /*TODO:Check if we need this later
        Ray ray = camera.ScreenPointToRay(rect);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)) {
            print("Ray hitting : " + hit.transform.gameObject + " at:" + hit.point);
        }
        Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow);*/
    }
    public void SetAvailableDarts(int count) {
        mAvailableDarts = count;
    }
    public int GetAvailableDarts() {
        return mAvailableDarts;
    }
    public void Fire() {
        if(mAvailableDarts <=0) {
            return;
        }
        switch (mBarrel) {
            case 0:
                mBarrel = 1;
                tLeft.transform.LookAt(crosshairProxy.transform);
                Instantiate(missilePrefab, tLeft.position,tLeft.rotation);
                break;
            case 1:
                mBarrel = 0;
                tRight.transform.LookAt(crosshairProxy.transform);
                Instantiate(missilePrefab, tRight.position, tRight.rotation);
                break;
        }
        --mAvailableDarts;
    }
    void OnGUI() {
        Vector3 rect = helicopterCamera.WorldToScreenPoint(crosshairProxy.transform.position);
        //Rect rp = new Rect(rect.x - 25, (Screen.height - (rect.y + 25)), 50, 50);
       // Rect rp = new Rect((Screen.width - 256)/2, (Screen.height - 256)/2, 256, 256);
        Rect rp = new Rect(rect.x-25, Screen.height -(rect.y + 25), 50, 50);

        //GUI.Label(new Rect(Screen.width / 2 - 25, Screen.height / 2 - 25, 50, 50), texture);
        if (mCrossHairVisible)
            GUI.Label(rp, crosshairTexture);
    }
    public void SetCrossHairVisibility(bool visibility) {
        mCrossHairVisible = visibility;
    }
    void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Vector3 crossHair = transform.position;
        crossHair.z += crosshairDistance;
        //Gizmos.DrawLine(transform.position, crosshairProxy.transform.position);
    }

}
