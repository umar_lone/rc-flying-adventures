﻿using UnityEngine;
using System.Collections;

public class DustTrigger : MonoBehaviour {

    public GameObject dustEffect;
    public GameObject waterEffect;
    private HelicopterController hc;
    private bool m_EffectActive = false;
    //private int checktakeoff = 1;
	// Use this for initialization
	void Start ()
    {
        hc = gameObject.GetComponentInParent<HelicopterController>();
        dustEffect.SetActive(false);
        waterEffect.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Field" && hc._engineForce > 10.0f)
        {
            dustEffect.transform.position = gameObject.transform.position;
            dustEffect.SetActive(true);
            m_EffectActive = true;
           // ++checktakeoff;
        }
        else if(other.tag == "Water" && hc._engineForce > 10.0f)
        {
            waterEffect.transform.position = gameObject.transform.position;
            waterEffect.SetActive(true);
            m_EffectActive = true;
        }
        else
            return;
        //mDustParticles = Instantiate(dustEffect, gameObject.transform.position, gameObject.transform.rotation) as GameObject;
    }

    void OnTriggerStay(Collider other)
    {
        if (m_EffectActive && other.tag == "Field")
        {
            if (hc._engineForce < 10.0f)
                Invoke("DeactivateDustEffect", 2);
            else if(hc._engineForce > 10.0f)
                dustEffect.SetActive(true);
            dustEffect.transform.position = gameObject.transform.position;         
        }
        else if(m_EffectActive && hc._engineForce > 10.0f && other.tag == "Water")
        {
            if (hc._engineForce < 10.0f)
                Invoke("DeactivateWaterEffect", 2);
            else if (hc._engineForce > 10.0f)
                waterEffect.SetActive(true);
            waterEffect.transform.position = gameObject.transform.position;
        }
        
        //This is not right. Don't get reference of the clone.
        // GameObject g = GameObject.Find("Dust Storm 1(Clone)");//.transform.position = gameObject.transform.position;
        // mDustParticles.transform.position = gameObject.transform.position;
        //dustEffect.position = gameObject.transform.position;
    }

    void OnTriggerExit(Collider other)
    {
        if (m_EffectActive &&  other.tag == "Field")
        {
            Invoke("DeactivateDustEffect", 2);
        }
        else if (m_EffectActive && other.tag == "Water")
        {
            Invoke("DeactivateWaterEffect", 2);
        }

        //if (b && other.tag == "Field")
        //{
        //    dustEffect.SetActive(false);
        //}
        //else if (b && other.tag == "Water")
        //{
        //    waterEffect.SetActive(false);
        //}
        //Invoke("WaitSecond", 5);
        //print("exit");
        //Destroy(GameObject.Find("Dust Storm 1(Clone)"), 5);
        //Destroy(GameObject.Find("BubblesStream05(Clone)"), 5);
    }
    void DeactivateDustEffect()
    {
        dustEffect.SetActive(false);
    }
    void DeactivateWaterEffect()
    {
        waterEffect.SetActive(false);

    }
   
}
