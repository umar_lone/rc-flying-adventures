﻿using UnityEngine;
using UnityEngine.UI;

public class HelicopterController : MonoBehaviour {
    private AudioSource mHeliAudio;
    public ControlPanel ControlPanel;
    public Rigidbody HelicopterModel;
    public HeliRotorController MainRotorController;
    public HeliRotorController SubRotorController;
    public GameObject rotorMainBlur;
    public GameObject rotorTailBlur;

    private GameObject mMainPropellor;
    private GameObject mTailPropellor;
    public Slider engineSlider;
    public float TurnForce = 3f;
    public float ForwardForce = 10f;
    public float ForwardTiltForce = 20f;
    public float TurnTiltForce = 30f;
    public float EffectiveHeight = 100f;

    public float turnTiltForcePercent = 1.5f;
    public float turnForcePercent = 1.3f;
    //public Text engineText;
    private bool keyPressed = false;

    //TODO Make these private later
    public float _engineForce;
    public bool mCrash = false;

    private const float LERPTIME = 10;
    private float currentLerpTime;
    private float currentSliderValue = 0;

    public float mainRotor;
    public float subRotor;
    //private float tempX, tempY;



    private Vector2 hMove = Vector2.zero;
    private Vector2 hTilt = Vector2.zero;
    private float hTurn = 0f;
    public bool IsOnGround = true;

    //Cannot control helicopter below this engine power
    private const int ENGINE_FORCE_THRESHOLD = 4; 

    // Use this for initialization
    void Start() {
        //TODO Uncomment later
        //transform.position = GameManager.Instance.GetVehiclePosition();
        //TODO #########################

        //Following code randomizes the helicopters rotation. It causes some unwanted rotation in helicopters
        //Will fix in next release.

        //Vector3 rotation = transform.eulerAngles;
        //rotation.y = Random.Range(0, 359);
        //transform.rotation = Quaternion.Euler(rotation.x, rotation.y, rotation.z);
        rotorMainBlur.SetActive(false);
        rotorTailBlur.SetActive(false);
        mMainPropellor = MainRotorController.gameObject.transform.FindChild("rotor_main").gameObject;
        mTailPropellor = SubRotorController.gameObject.transform.FindChild("rotor_tail").gameObject;
        mHeliAudio = GetComponent<AudioSource>();
        mHeliAudio.volume = GameManager.Instance.GetSoundSetting();
        mHeliAudio.pitch = 0f;
        mHeliAudio.Play();
        mHeliAudio.loop = true;
        GameManager.Instance.RegisterAudioSource(mHeliAudio);
        GameManager.Instance.SetCameraPosition(GameManager.Instance.GetCameraPosition());

#if UNITY_EDITOR
        ControlPanel.KeyPressed += OnKeyPressed;
#endif
    }
    public float EngineForce {
        get { return _engineForce; }
        set {

            MainRotorController.RotarSpeed = Mathf.Clamp(value * 100, 0, GameManager.Instance.GetTimeScale() > 1 ? 1000 : 1500);
            SubRotorController.RotarSpeed = Mathf.Clamp(value * 100, 0, GameManager.Instance.GetTimeScale() > 1 ? 1000 : 1500);
            mainRotor = MainRotorController.RotarSpeed;
            subRotor = SubRotorController.RotarSpeed;
            //TODO Investigate this further for smooth transition of pitch
            if (value <= 0) {
                mHeliAudio.pitch = 0;
            }
            else {
                mHeliAudio.pitch = value * 2 / 100;
            }
            _engineForce = value;

            if (_engineForce > 12) {
                mMainPropellor.GetComponent<MeshRenderer>().enabled = false; 
                mTailPropellor.GetComponent<MeshRenderer>().enabled = false;

                rotorMainBlur.SetActive(true);
                rotorTailBlur.SetActive(true);
            }
            else {
                mMainPropellor.GetComponent<MeshRenderer>().enabled = true; 
                mTailPropellor.GetComponent<MeshRenderer>().enabled = true;

                rotorMainBlur.SetActive(false);
                rotorTailBlur.SetActive(false);
            }
        }
    }
    public void Crash() {
        _engineForce = 0;
        mCrash = true;
    }
    void FixedUpdate() {
#if UNITY_EDITOR
        //CheckInput();
#else
			CheckInput ();
#endif
        if (_engineForce < ENGINE_FORCE_THRESHOLD) {
            return;
        }
        LiftProcess();
        MoveProcess();
        TiltProcess();
    }

    private void MoveProcess() {
        var turn = TurnForce * Mathf.Lerp(hMove.x, hMove.x * (turnTiltForcePercent - Mathf.Abs(hMove.y)), Mathf.Max(0f, hMove.y));
        hTurn = Mathf.Lerp(hTurn, turn, Time.fixedDeltaTime * TurnForce);
        /*
        This prevents the rotation of the helicopter when it is hovering.
        It is caused when the helicopter is turned left while moving
        forward.
        */
        if ((int)(Mathf.Abs(hTurn) * 100) < 4.7f) {
            hTurn = 0;
        }
        if ((int)(Mathf.Abs(hMove.y) * 100) < 2f) {
            hMove.y = 0;
        }
        HelicopterModel.AddRelativeTorque(0f, hTurn * HelicopterModel.mass, 0f);
        HelicopterModel.AddRelativeForce(Vector3.forward * Mathf.Max(0f, hMove.y * ForwardForce * HelicopterModel.mass));
    }

    private void LiftProcess() {
        var upForce = 1 - Mathf.Clamp(HelicopterModel.transform.position.y / EffectiveHeight, 0, 1);
        upForce = Mathf.Lerp(0f, EngineForce, upForce) * HelicopterModel.mass;
        HelicopterModel.AddRelativeForce(Vector3.up * upForce);
    }

    private void TiltProcess() {
        hTilt.x = Mathf.Lerp(hTilt.x, hMove.x * TurnTiltForce, Time.deltaTime);
        hTilt.y = Mathf.Lerp(hTilt.y, hMove.y * ForwardTiltForce, Time.deltaTime);
        //engineText.text = "turn : " + engineSlider.value + "\nhTurn : " + currentSliderValue;

        HelicopterModel.transform.localRotation = Quaternion.Euler(hTilt.y, HelicopterModel.transform.localEulerAngles.y, -hTilt.x);
    }

    private void OnKeyPressed(PressedKeyCode[] obj) {
        
        if (mCrash)
            return;
        
        float tempY = 0;
        float tempX = 0;

        // stable forward
        if (hMove.y > 0)
            tempY = -Time.fixedDeltaTime;
        else if (hMove.y < 0)
            tempY = Time.fixedDeltaTime;

        // stable lurn
        if (hMove.x > 0)
            tempX = -Time.fixedDeltaTime;
        else if (hMove.x < 0)
            tempX = Time.fixedDeltaTime;


        foreach (var pressedKeyCode in obj) {
            switch (pressedKeyCode) {
                case PressedKeyCode.SpeedUpPressed:
                    EngineForce += 0.1f;
                    break;
                case PressedKeyCode.SpeedDownPressed:
                    EngineForce -= 0.12f;
                    if (EngineForce < 0)
                        EngineForce = 0;
                    break;

                case PressedKeyCode.ForwardPressed:

                    if (IsOnGround)
                        break;
                    tempY = Time.fixedDeltaTime;
                    ForwardTiltForce = 1;
                    break;
                case PressedKeyCode.BackPressed:

                    if (IsOnGround)
                        break;
                    tempY = -Time.fixedDeltaTime;
                    ForwardTiltForce = 10;
                    break;
                case PressedKeyCode.LeftPressed:

                    if (IsOnGround)
                        break;
                    tempX = -Time.fixedDeltaTime;
                    break;
                case PressedKeyCode.RightPressed:

                    if (IsOnGround)
                        break;
                    tempX = Time.fixedDeltaTime;
                    break;
                case PressedKeyCode.TurnRightPressed: {
                        if (IsOnGround)
                            break;
                        var force = (turnForcePercent - Mathf.Abs(hMove.y)) * HelicopterModel.mass;
                        HelicopterModel.AddRelativeTorque(0f, force, 0);
                    }
                    break;
                case PressedKeyCode.TurnLeftPressed: {
                        if (IsOnGround)
                            break;

                        var force = -(turnForcePercent - Mathf.Abs(hMove.y)) * HelicopterModel.mass;
                        HelicopterModel.AddRelativeTorque(0f, force, 0);
                    }
                    break;

            }
        }
        SetMove(tempX, tempY);
    }

    private void OnCollisionEnter(Collision with) {
        //Maybe use this in future releases
        //ContactPoint [] points = with.contacts;
        //for(int i = 0; i < points.Length; ++i) {
        //    string pr = string.Format("{0} Point : {1} -> Normal : {2}", i, points[i].point, points[i].normal);
        //    print(pr);
        //}
        IsOnGround = true;
    }

    private void OnCollisionExit(Collision with) {
        IsOnGround = false;
    }

    void SetMove(float x, float y) {
        //Less engine force shouldn't let us control the helicopter
        if (_engineForce < ENGINE_FORCE_THRESHOLD) {
            return;
        }
        hMove.x += x;
        hMove.x = Mathf.Clamp(hMove.x, -1, 1);

        hMove.y += y;
        hMove.y = Mathf.Clamp(hMove.y, -1, 1);
    }

    public void CheckInput() {
        if (mCrash)
            return;

        float tempY = 0;
        float tempX = 0;
        currentSliderValue = Mathf.Lerp(currentSliderValue, engineSlider.value, Time.deltaTime);
        /*
        This ensures that the lerping does not take forever to reach the 
        target value. Otherwise, the helicopter just keeps on moving up or down
        until the currentSliderValue tries to match the engineSlider.value
        */
        if ((int)(currentSliderValue * 100) == (int)(engineSlider.value * 100)) {
            currentSliderValue = engineSlider.value;
        }
        //EngineForce = currentSliderValue * 100;
        float value = Mathf.Log10(currentSliderValue * 150);
        if (float.IsInfinity(value)) {
            value = 0;
        }
        value *= 10;
        float forceToApply;


        float hoveringValue = 18.7f;
        if (value < hoveringValue) {
            forceToApply = value;
            EngineForce = forceToApply;
        }
        else {
            forceToApply = hoveringValue + (Mathf.Abs(((currentSliderValue * 100) - 50)) * 1.6f);
            EngineForce = forceToApply;
        }
        //engineText.text = "Slider : " + engineSlider.value + "\nCurrent Value : " + forceToApply;

        StableMovement(ref tempY, ref tempX);
        if (!IsOnGround) {
            //if (ETCInput.GetButton("Left")) {
            //    tempX = -Time.fixedDeltaTime;
            //    keyPressed = true;
            //}
            //if (ETCInput.GetButton("Right")) {
            //    tempX = Time.fixedDeltaTime;
            //    keyPressed = true;
            //}
            //if (ETCInput.GetButton("Forward")) {
            //    tempY = Time.fixedDeltaTime;
            //    keyPressed = true;
            //}
            //if (ETCInput.GetButton("Backward")) {
            //    tempY = -Time.fixedDeltaTime;
            //    keyPressed = true;
            //}
            if (GameManager.Instance.Input.GetLeftButton()) {
                tempX = -Time.fixedDeltaTime;
                keyPressed = true;
            }
            if (GameManager.Instance.Input.GetRightButton()) {
                tempX = Time.fixedDeltaTime;
                keyPressed = true;
            }
            if (GameManager.Instance.Input.GetForwardButton()) {
                ForwardTiltForce = 1;
                tempY = Time.fixedDeltaTime;
                keyPressed = true;
            }
            if (GameManager.Instance.Input.GetBackwardButton()) {
                ForwardTiltForce = 10;
                tempY = -Time.fixedDeltaTime;
                keyPressed = true;
            }
        }

        SetMove(tempX, tempY);
    }

    public void OnMove(int type) {
        float tempY = 0;
        float tempX = 0;

        StableMovement(ref tempY, ref tempX);


        switch (type) {
            case 0://Left

                if (IsOnGround)
                    break;
                tempX = -Time.fixedDeltaTime;
                keyPressed = true;
                break;
            case 1://Right
                if (IsOnGround)
                    break;
                tempX = Time.fixedDeltaTime;
                keyPressed = true;
                break;
            case 2://Up
                if (IsOnGround)
                    break;
                tempY = Time.fixedDeltaTime;
                keyPressed = true;
                break;
            case 3://Down
                if (IsOnGround)
                    break;
                tempY = -Time.fixedDeltaTime;
                keyPressed = true;
                break;
            default:
                keyPressed = false;
                break;
        }
        SetMove(tempX, tempY);
    }

    void StableMovement(ref float tempY, ref float tempX) {
        // stable forward
        if (hMove.y > 0)
            tempY = -Time.fixedDeltaTime;
        else if (hMove.y < 0)
            tempY = Time.fixedDeltaTime;
        // stable lurn
        if (hMove.x > 0)
            tempX = -Time.fixedDeltaTime;
        else if (hMove.x < 0)
            tempX = Time.fixedDeltaTime;
    }
}