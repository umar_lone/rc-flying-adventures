﻿using UnityEngine;
using System.Collections;

public class WaterTrigger : MonoBehaviour {

    public Transform waterEffect;
    private HelicopterController hc;
    // Use this for initialization
    void Start()
    {

        hc = gameObject.GetComponentInParent<HelicopterController>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Water" && hc._engineForce > 10.0f)
            Instantiate(waterEffect, gameObject.transform.position, gameObject.transform.rotation);
        else
            return;


    }
    void OnTriggerStay(Collider other)
    {

    }
    void OnTriggerExit(Collider other)
    {
        Destroy(GameObject.Find("WaterHelicopter(Clone)"), 15);
    }

}
