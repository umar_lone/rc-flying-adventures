﻿using UnityEngine;
using System.Collections;

public class ClearLeaves : MonoBehaviour {
    public string objectiveMessage;
    public float force;
    public float radius;
    private GameObject player;
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(player != null) {
            Collider[] colliders = Physics.OverlapSphere(player.transform.position, 10);
            foreach (var c in colliders) {
                if(c.gameObject.tag == "Leaf") {
                    c.GetComponent<Rigidbody>().AddExplosionForce(force, player.transform.position,radius, 5);
                }
            }
        }
	}
    void OnTriggerEnter(Collider other) {
        if(other.tag == "Player") {
            UIManager.Instance.SetMissionMessage(objectiveMessage);
            player = other.gameObject;
           // GameManager.Instance.GetCurrentLevel().GetComponent<Level10Manager>().SetProgressBarVisibility(true);
        }
    }
}
